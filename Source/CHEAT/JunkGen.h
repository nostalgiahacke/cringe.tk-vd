#include <Windows.h>

namespace BestCheck
{
	int GetBestInt(int one, int two);
	float GetBestFloat(float one, bool check);
	float GetDelta();
	bool IsGood(int twoinone, bool automatic);
}