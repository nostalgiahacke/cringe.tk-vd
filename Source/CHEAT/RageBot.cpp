#include "RageBot.h"
#include "RenderManager.h"
#include "Resolver.h"
#include "Autowall.h"
#include <iostream>
#include "UTIL Functions.h"
#include "fakelaghelper.h"
#include "Global.h"
#include "interfaces.h"
#include "quickmaths.h" // 2+2 = selfharm
#include "movedata.h"
#include "nospreadhelper.h"
#include "Options.h"

void gshdgkjshgsuighsg::Init()
{

	IsAimStepping = false;
	IsLocked = false;
	TargetID = -1;
}

#define BEGIN_NAMESPACE( x ) namespace x {
#define END_NAMESPACE }

BEGIN_NAMESPACE(XorCompileTime)
constexpr auto time = __TIME__;
constexpr auto seed = static_cast<int>(time[7]) + static_cast<int>(time[6]) * 10 + static_cast<int>(time[4]) * 60 + static_cast<int>(time[3]) * 600 + static_cast<int>(time[1]) * 3600 + static_cast<int>(time[0]) * 36000;

// 1988, Stephen Park and Keith Miller
// "Random Number Generators: Good Ones Are Hard To Find", considered as "minimal standard"
// Park-Miller 31 bit pseudo-random number generator, implemented with G. Carta's optimisation:
// with 32-bit math and without division

template <int N>
struct RandomGenerator
{
private:
	static constexpr unsigned a = 16807; // 7^5
	static constexpr unsigned m = 2147483647; // 2^31 - 1

	static constexpr unsigned s = RandomGenerator<N - 1>::value;
	static constexpr unsigned lo = a * (s & 0xFFFF); // Multiply lower 16 bits by 16807
	static constexpr unsigned hi = a * (s >> 16); // Multiply higher 16 bits by 16807
	static constexpr unsigned lo2 = lo + ((hi & 0x7FFF) << 16); // Combine lower 15 bits of hi with lo's upper bits
	static constexpr unsigned hi2 = hi >> 15; // Discard lower 15 bits of hi
	static constexpr unsigned lo3 = lo2 + hi;

public:
	static constexpr unsigned max = m;
	static constexpr unsigned value = lo3 > m ? lo3 - m : lo3;
};

template <>
struct RandomGenerator<0>
{
	static constexpr unsigned value = seed;
};

template <int N, int M>
struct RandomInt
{
	static constexpr auto value = RandomGenerator<N + 1>::value % M;
};

template <int N>
struct RandomChar
{
	static const char value = static_cast<char>(1 + RandomInt<N, 0x7F - 1>::value);
};

template <size_t N, int K>
struct XorString
{
private:
	const char _key;
	std::array<char, N + 1> _encrypted;

	constexpr char enc(char c) const
	{
		return c ^ _key;
	}

	char dec(char c) const
	{
		return c ^ _key;
	}

public:
	template <size_t... Is>
	constexpr __forceinline XorString(const char* str, std::index_sequence<Is...>) : _key(RandomChar<K>::value), _encrypted{ enc(str[Is])... }
	{
	}

	__forceinline decltype(auto) decrypt(void)
	{
		for (size_t i = 0; i < N; ++i)
		{
			_encrypted[i] = dec(_encrypted[i]);
		}
		_encrypted[N] = '\0';
		return _encrypted.data();
	}
};

//--------------------------------------------------------------------------------
//-- Note: XorStr will __NOT__ work directly with functions like printf.
//         To work with them you need a wrapper function that takes a const char*
//         as parameter and passes it to printf and alike.
//
//         The Microsoft Compiler/Linker is not working correctly with variadic 
//         templates!
//  
//         Use the functions below or use std::cout (and similar)!
//--------------------------------------------------------------------------------

static auto w_printf = [](const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vprintf_s(fmt, args);
	va_end(args);
};

static auto w_printf_s = [](const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vprintf_s(fmt, args);
	va_end(args);
};

static auto w_sprintf = [](char* buf, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	int len = _vscprintf(fmt, args) // _vscprintf doesn't count  
		+ 1; // terminating '\0' 
	vsprintf_s(buf, len, fmt, args);
	va_end(args);
};

static auto w_sprintf_s = [](char* buf, size_t buf_size, const char* fmt, ...)
{
	va_list args;
	va_start(args, fmt);
	vsprintf_s(buf, buf_size, fmt, args);
	va_end(args);
};
static bool w_strcmp(const char* str1, const char* str2)
{
	return strcmp(str1, str2);
};
//#define XorStr( s ) ( XorCompileTime::XorString< sizeof( s ) - 1, __COUNTER__ >( s, std::make_index_sequence< sizeof( s ) - 1>() ).decrypt() )
/*#ifdef NDEBUG //nDEBUG
#define XorStr( s ) ( XorCompileTime::XorString< sizeof( s ) - 1, __COUNTER__ >( s, std::make_index_sequence< sizeof( s ) - 1>() ).decrypt() )
#elif*/
#define XorStr( s ) ( s )
//#endif

END_NAMESPACE

#include <stdio.h>
#include <string>
#include <iostream>

using namespace std;

class plbltei {
public:
	double vjvygza;
	bool tjchnjbrng;
	double nqteixho;
	double swibbwir;
	double vqkxjrhlyo;
	plbltei();
	bool euvczfsjpbzqdwb(double vmyzzcgqfdqjish, int psfubxzlkpmzlp, int wgkqcsavwbycvvk, bool wrmvmvnc, bool yxbzetxgec);
	void fbrtofccjnzzvhlrctsb(double oecovvw, bool xwdlrpkudsf, int zwxkjegiltacxq, string cqkxrj);
	double gxegwrlvrlkavlnnxobsmwqol(bool mkytetseakesh, string bnulfhdyywty);
	void pkdgfkzvabsz(string hbmvamebrrc, bool swcvhblxkxv);
	string ulybadmttqladkifp(string apjytfnvw, string ufnvinfbkmyq, int ssbaasfk, bool kbmopdsguljta, string tqvofgzeju, string kbxlherq, string boknczaeeihjeaq, int fdvuophddxjtue);
	void nondwyteuvekcmlqzofv(int rfheocfgi, double uhlrzrihm, int jveeslng, string vofeyfievng, int hfqqdcvdhxdtooa, double bmxyiirntpd, int kzfrfmwitfmz, double pkffri);
	string ykakppxttqswljlv(double vxcpj, string rvdkhpjguxt, string usmpjmeu);
	string xnqrmkxctnzpzienzbz(string hutprkm, bool nvgjagmcexoq, int uxpjjebdo, string uliutkbeknplg, bool vkuizxslpinrfxl, int qvjmfxsbend, double zumvdekvyrzcdr, double ncasvdfi, bool ncahbm);
	string tdmdrqyhudasneklt();
	void rpscpuduskpm(bool rtfuscs, string prubqctubrpth, string aicanlwxmy, double onehunpaeqop, string ybcibj, int lhckckdq, bool cjcregpbrlwwqx, bool wlhvmas);

protected:
	string fzinrcihqqopt;

	bool cifpsxmmbqbltdsqvxekr(double ppnvhdloqpvarlv, double kqmnxex, bool hxxitobjfisjrf, string sbmbq, string pardgvavzpde, string cengq, string gdybtmvqahfbt);
	int rsmdflartgdvuahyycfcpuw(int hzmdqxrcjch, string lrxxzwxpopoed);
	double upxhnjzoasqihumq(double hnnsokgffzrgej, bool kbzflw, string yuhxzscijki, int gxecbpyseollmyc, string akyhmx, int evueqccdkmbknl, double unlhlwhcuyrich, bool plepwiigcfwxxe, bool ayghyvtgtgm);
	void obclpbnjeshfnfek(int qnotfjipmlfs, double kyjefunhqwhao, int cdxfog, string pdwrbfbwlntv, string jirwp, bool crqwyubszflmoo, double xjkmwzfudqcozv, string lhtqb, bool usjez, int fzsrdbonozml);
	int zaxzaayhkklbrcqkmscaioehi();

private:
	string mwulngoiy;
	double rntvplx;

	double pfackbgzriggwioljdytu(double ucyyuq, double cqdowissweamfk, int wroqbrcyxrblmdf, double oqcppqfusj, bool dvchbgfjwpa, double gohbt, double uviedhthxbhe);

};



double plbltei::pfackbgzriggwioljdytu(double ucyyuq, double cqdowissweamfk, int wroqbrcyxrblmdf, double oqcppqfusj, bool dvchbgfjwpa, double gohbt, double uviedhthxbhe) {
	int fwlcbwpocrju = 5618;
	if (5618 == 5618) {
		int dbkds;
		for (dbkds = 98; dbkds > 0; dbkds--) {
			continue;
		}
	}
	if (5618 != 5618) {
		int lvll;
		for (lvll = 78; lvll > 0; lvll--) {
			continue;
		}
	}
	return 25557;
}

bool plbltei::cifpsxmmbqbltdsqvxekr(double ppnvhdloqpvarlv, double kqmnxex, bool hxxitobjfisjrf, string sbmbq, string pardgvavzpde, string cengq, string gdybtmvqahfbt) {
	double vzwatolcbwskifa = 40263;
	bool gntewamnbbgc = true;
	int xpjaidfawspm = 1588;
	string oikzsxjjxgbg = "efospffbtrgemdvqbfrbqkpesxldnasqcxfcucdeecaoemtnofyepavvmvhyinszfdymhass";
	int qcvwjlkapucujpj = 1399;
	return true;
}

int plbltei::rsmdflartgdvuahyycfcpuw(int hzmdqxrcjch, string lrxxzwxpopoed) {
	double gbyyjrdpvxyf = 15846;
	if (15846 == 15846) {
		int tgglgeciy;
		for (tgglgeciy = 1; tgglgeciy > 0; tgglgeciy--) {
			continue;
		}
	}
	if (15846 == 15846) {
		int jtqzokwusw;
		for (jtqzokwusw = 4; jtqzokwusw > 0; jtqzokwusw--) {
			continue;
		}
	}
	if (15846 == 15846) {
		int tsgfp;
		for (tsgfp = 90; tsgfp > 0; tsgfp--) {
			continue;
		}
	}
	return 81190;
}

double plbltei::upxhnjzoasqihumq(double hnnsokgffzrgej, bool kbzflw, string yuhxzscijki, int gxecbpyseollmyc, string akyhmx, int evueqccdkmbknl, double unlhlwhcuyrich, bool plepwiigcfwxxe, bool ayghyvtgtgm) {
	int tniqjkaosdak = 1321;
	double rzhmrn = 50535;
	if (50535 == 50535) {
		int yi;
		for (yi = 96; yi > 0; yi--) {
			continue;
		}
	}
	if (50535 == 50535) {
		int ads;
		for (ads = 94; ads > 0; ads--) {
			continue;
		}
	}
	if (1321 == 1321) {
		int dkfed;
		for (dkfed = 19; dkfed > 0; dkfed--) {
			continue;
		}
	}
	if (1321 == 1321) {
		int hkyd;
		for (hkyd = 1; hkyd > 0; hkyd--) {
			continue;
		}
	}
	return 90300;
}

void plbltei::obclpbnjeshfnfek(int qnotfjipmlfs, double kyjefunhqwhao, int cdxfog, string pdwrbfbwlntv, string jirwp, bool crqwyubszflmoo, double xjkmwzfudqcozv, string lhtqb, bool usjez, int fzsrdbonozml) {
	double nfkvrwfl = 35980;
	int twijq = 1925;
	bool zrmendmbxjwbejh = true;
	double yobhbyit = 28343;
	bool xnwwuqlyfcmwz = false;
	int faitbdztzbkh = 1786;
	if (false != false) {
		int ajouypua;
		for (ajouypua = 47; ajouypua > 0; ajouypua--) {
			continue;
		}
	}
	if (true != true) {
		int ppmwntdc;
		for (ppmwntdc = 8; ppmwntdc > 0; ppmwntdc--) {
			continue;
		}
	}
	if (true != true) {
		int ces;
		for (ces = 84; ces > 0; ces--) {
			continue;
		}
	}
	if (1786 != 1786) {
		int pfrrzwcwg;
		for (pfrrzwcwg = 28; pfrrzwcwg > 0; pfrrzwcwg--) {
			continue;
		}
	}

}

int plbltei::zaxzaayhkklbrcqkmscaioehi() {
	int hdaui = 1316;
	int grwmt = 2199;
	int jrcesm = 6991;
	double wdoemfb = 14343;
	double glmthxkwiatmwq = 24302;
	bool jgynxmbskkcnd = true;
	if (true == true) {
		int eol;
		for (eol = 22; eol > 0; eol--) {
			continue;
		}
	}
	if (6991 != 6991) {
		int xnh;
		for (xnh = 75; xnh > 0; xnh--) {
			continue;
		}
	}
	if (24302 == 24302) {
		int uhslodm;
		for (uhslodm = 81; uhslodm > 0; uhslodm--) {
			continue;
		}
	}
	return 4223;
}

bool plbltei::euvczfsjpbzqdwb(double vmyzzcgqfdqjish, int psfubxzlkpmzlp, int wgkqcsavwbycvvk, bool wrmvmvnc, bool yxbzetxgec) {
	double zjhvyuyhdek = 55243;
	bool yrqcpxcdkqu = true;
	if (true == true) {
		int jxryw;
		for (jxryw = 33; jxryw > 0; jxryw--) {
			continue;
		}
	}
	if (55243 == 55243) {
		int gnghxekek;
		for (gnghxekek = 89; gnghxekek > 0; gnghxekek--) {
			continue;
		}
	}
	if (true != true) {
		int rxfulw;
		for (rxfulw = 32; rxfulw > 0; rxfulw--) {
			continue;
		}
	}
	if (true == true) {
		int srblpcfluh;
		for (srblpcfluh = 77; srblpcfluh > 0; srblpcfluh--) {
			continue;
		}
	}
	if (55243 != 55243) {
		int kb;
		for (kb = 38; kb > 0; kb--) {
			continue;
		}
	}
	return true;
}

void plbltei::fbrtofccjnzzvhlrctsb(double oecovvw, bool xwdlrpkudsf, int zwxkjegiltacxq, string cqkxrj) {
	string itguoq = "ltidskzsgltubgforbhkqpgifrwxipvkwdlwotcnchauyibqojimwnczqlldakhzjedissoqktmnrblhjumvubx";
	string nhprnnegzga = "hvvnpdvczeyrlyrfkqvemvcwtpmqwjvhvsktgjbjntvwgpkzzfqkapsjzdztmkrjwaydyvq";
	bool hrrxaymnqnvayk = false;
	int hzyewue = 2074;
	string ajcquhfhje = "rtdejxeorpzaipinxcqdozycbrgouptlxikhs";
	string wimpn = "cmcnkozpudczvykvruekltwyhhbnwtxqiytwehrlfqbuadqqtythttb";
	double tjydlujsek = 53565;
	int arigstmv = 1338;
	string ybufcjy = "qcuvqhgsevdgxxqjgjmynhduesaxsblaoevrra";
	double pjpmgrrftjur = 984;
	if (false != false) {
		int selyewerwo;
		for (selyewerwo = 14; selyewerwo > 0; selyewerwo--) {
			continue;
		}
	}
	if (1338 == 1338) {
		int fshhry;
		for (fshhry = 68; fshhry > 0; fshhry--) {
			continue;
		}
	}
	if (53565 == 53565) {
		int dzexgokln;
		for (dzexgokln = 76; dzexgokln > 0; dzexgokln--) {
			continue;
		}
	}
	if (984 != 984) {
		int zcxlw;
		for (zcxlw = 65; zcxlw > 0; zcxlw--) {
			continue;
		}
	}
	if (2074 != 2074) {
		int yjmrjzch;
		for (yjmrjzch = 100; yjmrjzch > 0; yjmrjzch--) {
			continue;
		}
	}

}

double plbltei::gxegwrlvrlkavlnnxobsmwqol(bool mkytetseakesh, string bnulfhdyywty) {
	string lnfknvfpx = "hxofbqqwyuymccbqqwxxgkzyeskcenhxllznnlratylubuzuaxuea";
	string mpdtdwuynwlwn = "mnoyohynbjjjllsbgknszfmtbhchktetkuiokptgzxmlqikyoxltwavuvpzkeqqlmunbfsypwomvgbxyo";
	bool pramziblns = true;
	double wxusygic = 28356;
	int duslixxn = 1471;
	double cwyyrenvuzzzmic = 8094;
	if (string("hxofbqqwyuymccbqqwxxgkzyeskcenhxllznnlratylubuzuaxuea") != string("hxofbqqwyuymccbqqwxxgkzyeskcenhxllznnlratylubuzuaxuea")) {
		int cwugmxiocj;
		for (cwugmxiocj = 45; cwugmxiocj > 0; cwugmxiocj--) {
			continue;
		}
	}
	if (28356 != 28356) {
		int nvqsfyhp;
		for (nvqsfyhp = 0; nvqsfyhp > 0; nvqsfyhp--) {
			continue;
		}
	}
	return 45651;
}

void plbltei::pkdgfkzvabsz(string hbmvamebrrc, bool swcvhblxkxv) {
	bool cezhtnu = true;
	double yrbmgttsbzffudq = 844;
	int hmhibc = 3919;
	string mmtpvm = "yazzojtufribngawlznksqqvocijvmntihfvanfxnvbdegfrxicatvf";
	int ixounpvg = 4375;
	if (844 != 844) {
		int fkilmlnm;
		for (fkilmlnm = 97; fkilmlnm > 0; fkilmlnm--) {
			continue;
		}
	}
	if (true != true) {
		int tjyusey;
		for (tjyusey = 59; tjyusey > 0; tjyusey--) {
			continue;
		}
	}

}

string plbltei::ulybadmttqladkifp(string apjytfnvw, string ufnvinfbkmyq, int ssbaasfk, bool kbmopdsguljta, string tqvofgzeju, string kbxlherq, string boknczaeeihjeaq, int fdvuophddxjtue) {
	double efjamjmrmzzmsez = 19375;
	bool wezusqtvnkf = false;
	int uhhskcmtnusnyv = 96;
	int gbxwlupoq = 2314;
	int ivhsnmrob = 123;
	bool azcmwuwihjzkowe = true;
	string xtlvtfy = "vnvlcgycwqlapdglrina";
	bool ionkbknftmowtef = true;
	bool iwvghvt = false;
	int yghxozncltg = 1492;
	if (96 == 96) {
		int fdqvvrwyf;
		for (fdqvvrwyf = 23; fdqvvrwyf > 0; fdqvvrwyf--) {
			continue;
		}
	}
	if (false != false) {
		int hpbek;
		for (hpbek = 29; hpbek > 0; hpbek--) {
			continue;
		}
	}
	if (19375 == 19375) {
		int fi;
		for (fi = 49; fi > 0; fi--) {
			continue;
		}
	}
	if (96 != 96) {
		int gutcnahclz;
		for (gutcnahclz = 19; gutcnahclz > 0; gutcnahclz--) {
			continue;
		}
	}
	return string("tqzefvdigptgxaxeoe");
}

void plbltei::nondwyteuvekcmlqzofv(int rfheocfgi, double uhlrzrihm, int jveeslng, string vofeyfievng, int hfqqdcvdhxdtooa, double bmxyiirntpd, int kzfrfmwitfmz, double pkffri) {
	int wuhbbpxmy = 6496;
	int nkspkzxonpqibc = 3764;
	string qmsbpvzvcbas = "fxidvffkt";
	int sfipnn = 3840;
	if (6496 != 6496) {
		int ha;
		for (ha = 23; ha > 0; ha--) {
			continue;
		}
	}

}

string plbltei::ykakppxttqswljlv(double vxcpj, string rvdkhpjguxt, string usmpjmeu) {
	return string("selnjnankxmqogq");
}

string plbltei::xnqrmkxctnzpzienzbz(string hutprkm, bool nvgjagmcexoq, int uxpjjebdo, string uliutkbeknplg, bool vkuizxslpinrfxl, int qvjmfxsbend, double zumvdekvyrzcdr, double ncasvdfi, bool ncahbm) {
	double sycmskhv = 61811;
	if (61811 != 61811) {
		int jwzseafilg;
		for (jwzseafilg = 5; jwzseafilg > 0; jwzseafilg--) {
			continue;
		}
	}
	if (61811 == 61811) {
		int qrxlp;
		for (qrxlp = 95; qrxlp > 0; qrxlp--) {
			continue;
		}
	}
	if (61811 != 61811) {
		int ayrzsd;
		for (ayrzsd = 58; ayrzsd > 0; ayrzsd--) {
			continue;
		}
	}
	return string("rdzkiin");
}

string plbltei::tdmdrqyhudasneklt() {
	string qugjqu = "wduzmygpeuiwvrvqxizvlpnwqtwwfpqmscjmivvrz";
	string qfojve = "fczvsisglcljbzbqzkpxygvsosghkstrifzg";
	string srspbwri = "xcurpdvmhgmmmiwmukrfcltnjcvgidtsaqwcylqhjecgfanqztmddu";
	string qloazm = "rrzbcuxgwyzksqwkckihrafhxpdffgwimiqxwfgaqchafwnnyvjtljui";
	if (string("xcurpdvmhgmmmiwmukrfcltnjcvgidtsaqwcylqhjecgfanqztmddu") == string("xcurpdvmhgmmmiwmukrfcltnjcvgidtsaqwcylqhjecgfanqztmddu")) {
		int idzllgric;
		for (idzllgric = 22; idzllgric > 0; idzllgric--) {
			continue;
		}
	}
	if (string("rrzbcuxgwyzksqwkckihrafhxpdffgwimiqxwfgaqchafwnnyvjtljui") == string("rrzbcuxgwyzksqwkckihrafhxpdffgwimiqxwfgaqchafwnnyvjtljui")) {
		int rkojqkko;
		for (rkojqkko = 70; rkojqkko > 0; rkojqkko--) {
			continue;
		}
	}
	if (string("fczvsisglcljbzbqzkpxygvsosghkstrifzg") == string("fczvsisglcljbzbqzkpxygvsosghkstrifzg")) {
		int gdj;
		for (gdj = 42; gdj > 0; gdj--) {
			continue;
		}
	}
	if (string("xcurpdvmhgmmmiwmukrfcltnjcvgidtsaqwcylqhjecgfanqztmddu") != string("xcurpdvmhgmmmiwmukrfcltnjcvgidtsaqwcylqhjecgfanqztmddu")) {
		int jprcdzcid;
		for (jprcdzcid = 75; jprcdzcid > 0; jprcdzcid--) {
			continue;
		}
	}
	if (string("fczvsisglcljbzbqzkpxygvsosghkstrifzg") == string("fczvsisglcljbzbqzkpxygvsosghkstrifzg")) {
		int guiba;
		for (guiba = 19; guiba > 0; guiba--) {
			continue;
		}
	}
	return string("luewuilghmyf");
}

void plbltei::rpscpuduskpm(bool rtfuscs, string prubqctubrpth, string aicanlwxmy, double onehunpaeqop, string ybcibj, int lhckckdq, bool cjcregpbrlwwqx, bool wlhvmas) {
	bool sczstlhv = false;
	int wijmy = 1731;
	bool efkthn = true;
	int vusuzwjpdbiifw = 1221;
	double quygiscycjagut = 74152;
	int phmvxbm = 2842;
	bool fomruwrwlfk = true;
	double bllizqhtesk = 54059;
	int niydilpoghqszp = 2152;
	string cgdyapekn = "esvwoltxtotxtomarspvhmovkbsmhkcylkxecdsyeoujowpwxgbmhnlfycxgjvyhntn";
	if (1221 == 1221) {
		int kkgjvcnwrq;
		for (kkgjvcnwrq = 14; kkgjvcnwrq > 0; kkgjvcnwrq--) {
			continue;
		}
	}
	if (1731 == 1731) {
		int wkitfumcpu;
		for (wkitfumcpu = 73; wkitfumcpu > 0; wkitfumcpu--) {
			continue;
		}
	}
	if (false == false) {
		int snsyn;
		for (snsyn = 64; snsyn > 0; snsyn--) {
			continue;
		}
	}
	if (1221 == 1221) {
		int zfilkmcy;
		for (zfilkmcy = 24; zfilkmcy > 0; zfilkmcy--) {
			continue;
		}
	}

}

plbltei::plbltei() {
	this->euvczfsjpbzqdwb(3748, 230, 1897, true, false);
	this->fbrtofccjnzzvhlrctsb(12126, true, 617, string("jpqedlapyshbhkapsiemyrtbcxqjhxaastudopooqvmkbzwjcxaoozsbtjnhiqmshthblnohuuphfzaf"));
	this->gxegwrlvrlkavlnnxobsmwqol(false, string("bskfiiukux"));
	this->pkdgfkzvabsz(string("ujricdmqjlnhlnjdgqkbopetzyeozoxhdyddbpwxphmrpcjcbxqgfjbrhczhvftdhtvdbaynnhv"), false);
	this->ulybadmttqladkifp(string("yckduhwjdmjqbmdenhxsnawhhnrpspvsdaftzhqzfxor"), string("aqdiceemfpnkvydlcsxbbazoubfs"), 533, true, string("wsmkwpgskubsvupjxbptgcdblckjuznaixtdmbglbgajljkylztccpwutluauoucfgryikiwgklimgoqyxrgrfspkuxu"), string("qiddmtaprawadjfmbhmzstjqjlmgchpltyciyk"), string("yfocuvgpmyedopjxwwamprnvkinnxgvulazi"), 132);
	this->nondwyteuvekcmlqzofv(4595, 53549, 3048, string("myxwqeq"), 832, 67429, 3724, 2178);
	this->ykakppxttqswljlv(8483, string("pnxyhukiknyksebumoahieoiqyikeekfopakgijrllmowxegyzghzwwltzzmxocnumnsuspigkmafmvajzwixdsmszcsqsj"), string("dwassdjkvnnyomimwuuqsjaxxqqcxtyvswgqrmknwxrjnceiddpy"));
	this->xnqrmkxctnzpzienzbz(string("anxaqlnsiyvtvxrpgojsunidahowbgfeagfw"), false, 761, string("jgpsfty"), false, 7726, 39152, 35703, true);
	this->tdmdrqyhudasneklt();
	this->rpscpuduskpm(false, string("pftvsywdppktoqndtfvxbstfrhxyxlyvregeuftluvofojvlybobhhnfuxqqnlunhipkeqimdunrnmnhvxibhljxrilxlmcmmglv"), string("qtdgpzpfkjsxt"), 8944, string("rjeqfknppkaeshuijyrdpnxrxlqjvaftjbgoplmdjughbblkgdsdtvwnpxkmb"), 5938, true, false);
	this->cifpsxmmbqbltdsqvxekr(63929, 10311, true, string("wxckpcrbwwqikyttwybaydidofjvhiopzndrdgnntwbgesdkbictubdjokqbdwbsnsrnvjqhdfzilhgeycmrljivjuaq"), string("tmkwpirdxhxjygcwmnph"), string("qxtfvixxxwgmgzcldxtufgulovkognv"), string("uupfuqibzwqppqpqsidfahzgyzcbfrdomfdulkuhacqfusp"));
	this->rsmdflartgdvuahyycfcpuw(806, string("oybtnqfzaqknmycniwgvzcntbsobgcqztnwkyosssdzhcsfwkhbe"));
	this->upxhnjzoasqihumq(4562, true, string("ljoxoh"), 8088, string("zdwpoldavglosgqopbetgdumbndqahqxxinmfvliuqrnafnfbgiyqgkhiddaxidtuegzhu"), 426, 21962, false, false);
	this->obclpbnjeshfnfek(923, 74708, 3221, string("xfkhilfsucmiemqrkaatmqxwbylyqxoqunkxypevqgfwfsjzsebz"), string("pcueprwztihrbvtqenwzmqvjhxldlhkkwwxkp"), false, 6914, string("izslitchfaenunjqcgsvzlokamctuzkexajeuscjoakmkhkggwxmrzlmeremxbuvomghepfksqmlldztvxcfqjaptjoaw"), false, 0);
	this->zaxzaayhkklbrcqkmscaioehi();
	this->pfackbgzriggwioljdytu(55308, 23102, 7083, 56163, false, 11221, 9134);
}



void gshdgkjshgsuighsg::PredictBypass()
{
	float djghdkgfhdgjkdhg = 967858;
	djghdkgfhdgjkdhg = 3573478;
	if (djghdkgfhdgjkdhg = 48684)
		djghdkgfhdgjkdhg = 85723;
	djghdkgfhdgjkdhg = 34637;
	djghdkgfhdgjkdhg = 5688;
	if (djghdkgfhdgjkdhg = 484626);
	djghdkgfhdgjkdhg = 45748;
	djghdkgfhdgjkdhg = 8458737;
	if (djghdkgfhdgjkdhg = 23626)
		djghdkgfhdgjkdhg = 374588;
	djghdkgfhdgjkdhg = 485736;
	djghdkgfhdgjkdhg = 26357;
	if (djghdkgfhdgjkdhg = 45747);
	djghdkgfhdgjkdhg = 3477;
	djghdkgfhdgjkdhg = 83563;
	if (djghdkgfhdgjkdhg = 346388)
		djghdkgfhdgjkdhg = 378262;
	djghdkgfhdgjkdhg = 35738;
	djghdkgfhdgjkdhg = 849485737;
	if (djghdkgfhdgjkdhg = 83234);
	djghdkgfhdgjkdhg = 346377;
	djghdkgfhdgjkdhg = 4754736;
}

void gshdgkjshgsuighsg::Prediction(CUserCmd* pCmd, IClientEntity* LocalPlayer)
{
	PredictBypass();
	if (Interfaces::MoveHelper && options.Rage.Enabled && options.Rage.PosAdjustment && LocalPlayer->IsAlive())
	{
		PredictBypass();
		float curtime = Interfaces::Globals->curtime;
		float frametime = Interfaces::Globals->frametime;
		int iFlags = LocalPlayer->GetFlags();
		PredictBypass();
		Interfaces::Globals->curtime = (float)LocalPlayer->GetTickBase() * Interfaces::Globals->interval_per_tick;
		Interfaces::Globals->frametime = Interfaces::Globals->interval_per_tick;
		PredictBypass();
		Interfaces::MoveHelper->SetHost(LocalPlayer);
		PredictBypass();
		Interfaces::Prediction1->SetupMove(LocalPlayer, pCmd, nullptr, bMoveData);
		Interfaces::GameMovement->ProcessMovement(LocalPlayer, bMoveData);
		Interfaces::Prediction1->FinishMove(LocalPlayer, pCmd, bMoveData);
		PredictBypass();
		Interfaces::MoveHelper->SetHost(0);
		PredictBypass();
		Interfaces::Globals->curtime = curtime;
		Interfaces::Globals->frametime = frametime;
		PredictBypass();
	}
	PredictBypass();
}



float InterpFix()
{

		static ConVar* cvar_cl_interp = Interfaces::CVar->FindVar("cl_interp");
		static ConVar* cvar_cl_updaterate = Interfaces::CVar->FindVar("cl_updaterate");
		static ConVar* cvar_sv_maxupdaterate = Interfaces::CVar->FindVar("sv_maxupdaterate");
		static ConVar* cvar_sv_minupdaterate = Interfaces::CVar->FindVar("sv_minupdaterate");
		static ConVar* cvar_cl_interp_ratio = Interfaces::CVar->FindVar("cl_interp_ratio");

		IClientEntity* pLocal = hackManager.pLocal();
		CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());

		float cl_interp = cvar_cl_interp->GetFloat();
		int cl_updaterate = cvar_cl_updaterate->GetInt();
		int sv_maxupdaterate = cvar_sv_maxupdaterate->GetInt();
		int sv_minupdaterate = cvar_sv_minupdaterate->GetInt();
		int cl_interp_ratio = cvar_cl_interp_ratio->GetInt();

		if (sv_maxupdaterate <= cl_updaterate)
			cl_updaterate = sv_maxupdaterate;

		if (sv_minupdaterate > cl_updaterate)
			cl_updaterate = sv_minupdaterate;

		float new_interp = (float)cl_interp_ratio / (float)cl_updaterate;

		if (new_interp > cl_interp)
			cl_interp = new_interp;

		return max(cl_interp, cl_interp_ratio / cl_updaterate);
	
	// 
}

void FakeWalk(CUserCmd * pCmd, bool & bSendPacket)
{

	IClientEntity* pLocal = hackManager.pLocal();
	if (GetAsyncKeyState(VK_SHIFT))
	{

		static int iChoked = -1;
		iChoked++;

		if (iChoked < 1)
		{
			bSendPacket = false;



			pCmd->tick_count += 7.95; // 10.95
			pCmd->command_number += 3.07 + pCmd->tick_count % 2 ? 0 : 1; // 5
			pCmd->buttons |= pLocal->GetMoveType() == IN_FORWARD;
			pCmd->buttons |= pLocal->GetMoveType() == IN_LEFT;
			pCmd->buttons |= pLocal->GetMoveType() == IN_BACK;
			pCmd->forwardmove = pCmd->sidemove = 0.f;
		}
		else
		{
			bSendPacket = true;
			iChoked = -1;

			Interfaces::Globals->frametime *= (pLocal->GetVelocity().Length2D()) / 6; // 10
			pCmd->buttons |= pLocal->GetMoveType() == IN_FORWARD;
		}
	}
}

void gshdgkjshgsuighsg::Draw()
{
	PredictBypass();
}

bool IsAbleToShoot(IClientEntity* pLocal)
{
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	if (!pLocal)return false;
	if (!pWeapon)return false;
	float flServerTime = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;
	return (!(pWeapon->GetNextPrimaryAttack() > flServerTime));
}

float hitchance(IClientEntity* pLocal, CBaseCombatWeapon* pWeapon)
{
	float hitchance = 101;
	if (!pWeapon) return 0;
	if (options.Rage.hitchanceval > 1)
	{
		float inaccuracy = pWeapon->GetInaccuracy();
		if (inaccuracy == 0) inaccuracy = 0.0000000000001;
		inaccuracy = 1 / inaccuracy;
		hitchance = inaccuracy;
	}
	return hitchance;
}

bool CanOpenFire() 
{
	IClientEntity* pLocalEntity = (IClientEntity*)Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());
	if (!pLocalEntity)
		return false;

	CBaseCombatWeapon* entwep = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(pLocalEntity->GetActiveWeaponHandle());

	float flServerTime = (float)pLocalEntity->GetTickBase() * Interfaces::Globals->interval_per_tick;
	float flNextPrimaryAttack = entwep->GetNextPrimaryAttack();

	std::cout << flServerTime << " " << flNextPrimaryAttack << std::endl;

	return !(flNextPrimaryAttack > flServerTime);
}

void gshdgkjshgsuighsg::Move(CUserCmd *pCmd, bool &bSendPacket)
{
	PredictBypass();
	IClientEntity* pLocalEntity = (IClientEntity*)Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());
	PredictBypass();
	if (!pLocalEntity || !options.Rage.MainSwitch || !Interfaces::Engine->IsConnected() || !Interfaces::Engine->IsInGame())
		return;
	PredictBypass();
	if (options.Rage.enableaa)
	{
		PredictBypass();
		static int ChokedPackets = -1;
		PredictBypass();
		CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());
		if (!pWeapon)
			return;
		PredictBypass();
		if (ChokedPackets < 1 && pLocalEntity->IsAlive() && pCmd->buttons & IN_ATTACK && CanOpenFire() && GameUtils::IsBallisticWeapon(pWeapon))
		{
			bSendPacket = false;
			PredictBypass();
		}
		else
		{
			PredictBypass();
			if (pLocalEntity->IsAlive())
			{
				PredictBypass();
				DoAntiAim(pCmd, bSendPacket);
				PredictBypass();

			}
			PredictBypass();
			ChokedPackets = 1;
		}
		PredictBypass();
	}
	PredictBypass();
	if (options.Rage.Enabled)
		DoAimbot(pCmd, bSendPacket);
	PredictBypass();
	if (options.Rage.AcurracyControl == 0)
	{
		PredictBypass();
	}
	else if (options.Rage.AcurracyControl == 1)
	{
		PredictBypass();
		DoNoRecoil(pCmd);
		PredictBypass();
	}
	else if (options.Rage.AcurracyControl == 3) {
		PredictBypass();
		DoNoRecoil(pCmd);
		PredictBypass();
	}

	if (options.Rage.lagfix)
		pCmd->tick_count = TIME_TO_TICKS(InterpFix());
	PredictBypass();
	if (Menu::Window.RageBotTab.AimbotAimStep.GetState())
	{
		PredictBypass();
		Vector AddAngs = pCmd->viewangles - LastAngle;
		if (AddAngs.Length2D() > 25.f)
		{
			PredictBypass();
			Normalize(AddAngs, AddAngs);
			AddAngs *= 23;
			pCmd->viewangles = LastAngle + AddAngs;
			GameUtils::NormaliseViewAngle(pCmd->viewangles);
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	LastAngle = pCmd->viewangles;
	PredictBypass();
}

Vector BestPoint(IClientEntity *targetPlayer, Vector &final)
{
	IClientEntity* pLocal = hackManager.pLocal();
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());

	//PredictBypass();
	trace_t tr;
	Ray_t ray;
	CTraceFilter filter;
	//PredictBypass();
	filter.pSkip = targetPlayer;
	ray.Init(final + Vector(0, 0,10), final); // all 0 or 0,0,10, either way, should hit the top of the head, but its weird af.
	Interfaces::Trace->TraceRay(ray, MASK_SHOT, &filter, &tr);
	//PredictBypass();
	// if you're actually one to look through this stuff, do tag me @ discord and tell me.
	//PredictBypass();
	final = tr.endpos;
	return final;
	//PredictBypass();
}

bool gshdgkjshgsuighsg::extra(bool &bSendPacket)
{
	PredictBypass();
	static int ChokedPackets = -1;
	ChokedPackets++;
	bool canchoke;
	PredictBypass();

	if (Menu::Window.RageBotTab.chokepackets.GetState())
		canchoke = true;
	else
		canchoke = false;
	PredictBypass();
	if (canchoke == true)
		return ChokedPackets = 2;
	else
		return ChokedPackets = 0;
	PredictBypass();
}

void gshdgkjshgsuighsg::DoAimbot(CUserCmd *pCmd, bool &bSendPacket)
{
	PredictBypass();

	static int ChokedPackets = -1;
	ChokedPackets++;
	IClientEntity* pTarget = nullptr;
	IClientEntity* pLocal = hackManager.pLocal();
	Vector Start = pLocal->GetViewOffset() + pLocal->GetOrigin();
	bool FindNewTarget = true;
	CSWeaponInfo* weapInfo = ((CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle()))->GetCSWpnData();
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	PredictBypass();
	Vector Point;


	PredictBypass();
	if (Menu::Window.RageBotTab.AutoRevolver.GetState())
		if (GameUtils::IsRevolver(pWeapon))
		{
			PredictBypass();
			static int delay = 0;
			delay++;
			if (delay <= 15)pCmd->buttons |= IN_ATTACK;
			else delay = 0;
			PredictBypass();
		}
	if (pWeapon)
	{
		if (pWeapon->GetAmmoInClip() == 0 || !GameUtils::IsBallisticWeapon(pWeapon)) return;
		PredictBypass();
	}
	else return;
	if (IsLocked && TargetID >= 0 && HitBox >= 0)
	{
		PredictBypass();
		pTarget = Interfaces::EntList->GetClientEntity(TargetID);
		if (pTarget  && TargetMeetsRequirements(pTarget))
		{
			PredictBypass();
			HitBox = HitScan(pTarget);
			if (HitBox >= 0)
			{
				PredictBypass();
				Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset(), View;
				Interfaces::Engine->GetViewAngles(View);
				float FoV = FovToPlayer(ViewOffset, View, pTarget, HitBox);
				if (FoV < options.Rage.AimFOV)	FindNewTarget = false;
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}


	if (FindNewTarget)
	{
		PredictBypass();
		TargetID = 0;
		pTarget = nullptr;
		HitBox = -1;
		switch (options.Rage.selectiontrgt)
		{
		case 0:TargetID = GetTargetCrosshair(); break;
		case 1:TargetID = GetTargetDistance(); break;
		case 2:TargetID = GetTargetHealth(); break;
		case 3:TargetID = GetTargetThreat(pCmd); break;
		case 4:TargetID = GetTargetNextShot(); break;
		}
		PredictBypass();
		if (TargetID >= 0) pTarget = Interfaces::EntList->GetClientEntity(TargetID);
		else
		{

			PredictBypass();
			pTarget = nullptr;
			HitBox = -1;
			PredictBypass();
		}
		PredictBypass();
	} 
	PredictBypass();
	Globals::Target = pTarget;
	Globals::TargetID = TargetID;
	if (TargetID >= 0 && pTarget)
	{
		PredictBypass();
		HitBox = HitScan(pTarget);
		PredictBypass();
		if (!CanOpenFire()) return;
		PredictBypass();
		if (Menu::Window.RageBotTab.AimbotKeyPress.GetState())
		{
			PredictBypass();

			int Key = Menu::Window.RageBotTab.AimbotKeyBind.GetKey();
			PredictBypass();
			if (Key >= 0 && !GUI.GetKeyState(Key))
			{
				PredictBypass();
				TargetID = -1;
				pTarget = nullptr;
				HitBox = -1;
				PredictBypass();
				return;
				PredictBypass();
			}
			PredictBypass();
		}
		float pointscale = options.Rage.Pointscale + 0.1f;
		Vector AimPoint = GetHitboxPosition(pTarget, HitBox) + Vector(0, 0, pointscale);
		if (options.Rage.MultiPoint_mb) Point = BestPoint(pTarget, AimPoint);
		else Point = AimPoint;
		PredictBypass();
		if (GameUtils::IsScopedWeapon(pWeapon) && !pWeapon->IsScoped() && Menu::Window.RageBotTab.AccuracyAutoScope.GetState()) pCmd->buttons |= IN_ATTACK2;
		else if ((options.Rage.hitchanceval * 1.5 <= hitchance(pLocal, pWeapon)) || options.Rage.hitchanceval == 0 || *pWeapon->m_AttributeManager()->m_Item()->ItemDefinitionIndex() == 64)
			{
			PredictBypass();
			if (pTarget->GetChokedTicks() > 4)
			{
				if (options.Rage.AutoFiring && !(pCmd->buttons & IN_ATTACK))
				{
					PredictBypass();
					pCmd->buttons |= IN_ATTACK;
					AimPoint -= pTarget->GetAbsOrigin();
					AimPoint += pTarget->GetNetworkOrigin();
					PredictBypass();
				}
				else if (pCmd->buttons & IN_ATTACK || pCmd->buttons & IN_ATTACK2)return;
			}
			else if (AimAtPoint(pLocal, Point, pCmd, bSendPacket))
					if (options.Rage.AutoFiring && !(pCmd->buttons & IN_ATTACK))pCmd->buttons |= IN_ATTACK;
					else if (pCmd->buttons & IN_ATTACK || pCmd->buttons & IN_ATTACK2)return;
					PredictBypass();
			}
		if (IsAbleToShoot(pLocal) && pCmd->buttons & IN_ATTACK) { Globals::Shots += 1;  ChokedPackets = 1; }
	}
	PredictBypass();

}

bool gshdgkjshgsuighsg::TargetMeetsRequirements(IClientEntity* pEntity)
{
	PredictBypass();
	if (pEntity && pEntity->IsDormant() == false && pEntity->IsAlive() && pEntity->GetIndex() != hackManager.pLocal()->GetIndex())
	{
		PredictBypass();
		ClientClass *pClientClass = pEntity->GetClientClass();
		player_info_t pinfo;
		if (pClientClass->m_ClassID == (int)CSGOClassID::CCSPlayer && Interfaces::Engine->GetPlayerInfo(pEntity->GetIndex(), &pinfo))
		{
			if (pEntity->GetTeamNum() != hackManager.pLocal()->GetTeamNum() || Menu::Window.RageBotTab.TargetFriendlyFire.GetState())
			{
				if (!pEntity->HasGunGameImmunity())
				{
					return true;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return false;
	PredictBypass();
}

float gshdgkjshgsuighsg::FovToPlayer(Vector ViewOffSet, Vector View, IClientEntity* pEntity, int aHitBox)
{
	CONST FLOAT MaxDegrees = 180.0f;
	PredictBypass();
	Vector Angles = View;
	PredictBypass();
	Vector Origin = ViewOffSet;
	PredictBypass();
	Vector Delta(0, 0, 0);
	PredictBypass();
	Vector Forward(0, 0, 0);
	PredictBypass();
	AngleVectors(Angles, &Forward);
	Vector AimPos = GetHitboxPosition(pEntity, aHitBox);
	PredictBypass();
	VectorSubtract(AimPos, Origin, Delta);
	PredictBypass();
	Normalize(Delta, Delta);
	PredictBypass();
	FLOAT DotProduct = Forward.Dot(Delta);
	PredictBypass();
	return (acos(DotProduct) * (MaxDegrees / PI));
	PredictBypass();
}

int gshdgkjshgsuighsg::GetTargetCrosshair()
{
	PredictBypass();
	int target = -1;
	float minFoV = options.Rage.AimFOV;
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector View; Interfaces::Engine->GetViewAngles(View);
	PredictBypass();
	for (int i = 0; i < Interfaces::EntList->GetMaxEntities(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			int NewHitBox = HitScan(pEntity);
			if (NewHitBox >= 0)
			{
				PredictBypass();
				float fov = FovToPlayer(ViewOffset, View, pEntity, 0);
				if (fov < minFoV)
				{
					PredictBypass();
					minFoV = fov;
					target = i;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return target;
	PredictBypass();
}

int gshdgkjshgsuighsg::GetTargetDistance()
{
	PredictBypass();
	int target = -1;
	int minDist = 99999;
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector View; Interfaces::Engine->GetViewAngles(View);
	PredictBypass();
	for (int i = 0; i < Interfaces::EntList->GetMaxEntities(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			int NewHitBox = HitScan(pEntity);
			if (NewHitBox >= 0)
			{
				PredictBypass();
				Vector Difference = pLocal->GetOrigin() - pEntity->GetOrigin();
				int Distance = Difference.Length();
				float fov = FovToPlayer(ViewOffset, View, pEntity, 0);
				if (Distance < minDist && fov < options.Rage.AimFOV)
				{
					PredictBypass();
					minDist = Distance;
					target = i;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return target;
	PredictBypass();
}

int gshdgkjshgsuighsg::GetTargetNextShot()
{
	int target = -1;
	int minfov = 361;
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector View; Interfaces::Engine->GetViewAngles(View);
	PredictBypass();
	for (int i = 0; i < Interfaces::EntList->GetMaxEntities(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			int NewHitBox = HitScan(pEntity);
			if (NewHitBox >= 0)
			{
				PredictBypass();
				int Health = pEntity->GetHealth();
				float fov = FovToPlayer(ViewOffset, View, pEntity, 0);
				if (fov < minfov && fov < options.Rage.AimFOV)
				{
					minfov = fov;
					target = i;
					PredictBypass();
				}
				else
					minfov = 361;
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return target;
	PredictBypass();
}

float GetFov(const QAngle& viewAngle, const QAngle& aimAngle)
{
	Vector ang, aim;

	AngleVectors(viewAngle, &aim);
	AngleVectors(aimAngle, &ang);

	return RAD2DEG(acos(aim.Dot(ang) / aim.LengthSqr()));
}

double inline __declspec (naked) __fastcall FASTSQRT(double n)
{
	_asm fld qword ptr[esp + 4]
		_asm fsqrt
	_asm ret 8
}

float VectorDistance(Vector v1, Vector v2)
{
	return FASTSQRT(pow(v1.x - v2.x, 2) + pow(v1.y - v2.y, 2) + pow(v1.z - v2.z, 2));
}

int gshdgkjshgsuighsg::GetTargetThreat(CUserCmd* pCmd)
{
	PredictBypass();
	auto iBestTarget = -1;
	float flDistance = 8192.f;
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	PredictBypass();
	for (int i = 0; i < Interfaces::EntList->GetHighestEntityIndex(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			int NewHitBox = HitScan(pEntity);
			auto vecHitbox = pEntity->GetBonePos(NewHitBox);
			if (NewHitBox >= 0)
			{
				PredictBypass();
				Vector Difference = pLocal->GetOrigin() - pEntity->GetOrigin();
				QAngle TempTargetAbs;
				CalcAngle(pLocal->GetEyePosition(), vecHitbox, TempTargetAbs);
				float flTempFOVs = GetFov(pCmd->viewangles, TempTargetAbs);
				float flTempDistance = VectorDistance(pLocal->GetOrigin(), pEntity->GetOrigin());
				PredictBypass();
				if (flTempDistance < flDistance && flTempFOVs < options.Rage.AimFOV)
				{
					flDistance = flTempDistance;
					iBestTarget = i;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return iBestTarget;
	PredictBypass();
}

int gshdgkjshgsuighsg::GetTargetHealth()
{
	PredictBypass();
	int target = -1;
	int minHealth = 101;
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	Vector View; Interfaces::Engine->GetViewAngles(View);
	PredictBypass();

	for (int i = 0; i < Interfaces::EntList->GetMaxEntities(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			int NewHitBox = HitScan(pEntity);
			if (NewHitBox >= 0)
			{
				PredictBypass();
				int Health = pEntity->GetHealth();
				float fov = FovToPlayer(ViewOffset, View, pEntity, 0);
				if (Health < minHealth && fov < options.Rage.AimFOV)
				{
					minHealth = Health;
					target = i;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return target;
	PredictBypass();
}

int gshdgkjshgsuighsg::HitScan(IClientEntity* pEntity)
{
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	std::vector<int> HitBoxesToScan;
	PredictBypass();
#pragma region GetHitboxesToScan
	int huso = (pEntity->GetHealth());
	int health = options.Rage.BaimIFUnderXHP;
	bool bBaim = PlayerList[pEntity->GetIndex()].bBaim;
	bool bPriotit = PlayerList[pEntity->GetIndex()].bHighPriority;
	PredictBypass();
	bool AWall = options.Rage.autowall;
	bool Multipoint = options.Rage.MultiPoint_mb;
	int TargetHitbox = options.Rage.Hitbone1;
	static bool enemyHP = false;
	IClientEntity* WeaponEnt = Interfaces::EntList->GetClientEntityFromHandle(pLocal->GetActiveWeaponHandle());
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());
	int xs;
	int ys;
	Interfaces::Engine->GetScreenSize(xs, ys);
	xs /= 2; ys /= 2;
	PredictBypass();
	auto accuracy = pWeapon->GetInaccuracy() * 550.f; //3000
	PredictBypass();

	int AimbotBaimOnKey = Menu::Window.RageBotTab.AimbotBaimOnKey.GetKey();
	if (AimbotBaimOnKey >= 0 && GUI.GetKeyState(AimbotBaimOnKey))
	{
		PredictBypass();
		HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach); // 4
		HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh); // 9
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh); // 8
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot); // 13
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot); // 12
		PredictBypass();
	}

	PredictBypass();
	if (huso < health)
	{
		HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LowerChest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
		PredictBypass();
	}
	else if (bBaim)
	{
		HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LowerChest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
	}
	else if (bPriotit)
	{
		CUserCmd* pCmd;
		pCmd->tick_count = TIME_TO_TICKS(*(float*)((DWORD)pEntity + 0x04C8)); TIME_TO_TICKS(InterpFix());
		HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
	}
	else if (Globals::Shots >= options.Rage.BaimAfterXShots)
	{
		HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
		HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
		PredictBypass();
	}
	else if (Menu::Window.RageBotTab.AWPAtBody.GetState() && GameUtils::AWP(pWeapon))
	{
		PredictBypass();
		HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
		HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
		HitBoxesToScan.push_back((int)CSGOHitboxID::LowerChest);
		HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
		PredictBypass();
	}
	else if (TargetHitbox)
	{
		PredictBypass();
		switch (options.Rage.Hitbone1)
		{
		case 0:
		{ /* wont shoot anything at all. Peace and love bruuuuh */ } break;
		case 1:
			HitBoxesToScan.push_back((int)CSGOHitboxID::Head); // 1
			HitBoxesToScan.push_back((int)CSGOHitboxID::Neck); // lets compensate a bit
			PredictBypass();
			if (options.Rage.preferbaim != 0)
			{
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
				PredictBypass();
			}
			else
			{
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
				PredictBypass();
			}
			break;
		case 2:
		{
			PredictBypass();
			switch (options.Rage.preferbaim)
			{
			case 0:
			{
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
				PredictBypass();
			}
			PredictBypass();
			break;
			case 1:
			{
				PredictBypass();
				if (accuracy > 60)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			break;
			case 2:
			{
				PredictBypass();
				if (Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			break;
			case 3:
			{
				PredictBypass();
				if (accuracy >= 40)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else if (accuracy >= 40 && Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			break;
			case 4:
			{
				PredictBypass();
				{
					PredictBypass();
					if (accuracy >= 5)
					{
						PredictBypass();
						HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
						HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
						HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
						PredictBypass();
					}
					else if (accuracy >= 4 && Globals::Shots > 1)
					{
						HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
						PredictBypass();
					}
					else
					{
						PredictBypass();

						HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
						HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
						HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
						HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
						HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
						HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
						HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
						PredictBypass();
					}
					PredictBypass();
				}
				PredictBypass();
			}
			break;
			}
			PredictBypass();
			break;
			PredictBypass();
		}
		PredictBypass();
		break;
		PredictBypass();
		case 3:
		{
			PredictBypass();
			switch (options.Rage.preferbaim)
			{
				PredictBypass();
			case 0:
			{
				PredictBypass();
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
				HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
				PredictBypass();
			}
			PredictBypass();
			break;
			case 1:
			{
				PredictBypass();
				if (accuracy > 60)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			break;
			case 2:
			{
				PredictBypass();
				if (Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			break;
			case 3:
			{
				PredictBypass();
				if (accuracy >= 40)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else if (accuracy >= 40 && Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			break;
			PredictBypass();
			}
			PredictBypass();
			break;
			PredictBypass();
		}

		break;
		case 4:
		{

			PredictBypass();
			switch (options.Rage.preferbaim)
			{
				PredictBypass();
			case 0:
			{
				PredictBypass();
				HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
				HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
				HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
				HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
				HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
				HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
				PredictBypass();
			}
			PredictBypass();
			break;
			case 1:
			{
				PredictBypass();
				if (accuracy > 60)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
					PredictBypass();
				}
				PredictBypass();
			}
			break;
			PredictBypass();
			case 2:
			{
				PredictBypass();
				if (Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
					PredictBypass();
				}
				PredictBypass();
			}
			case 3:
			{
				PredictBypass();
				if (accuracy >= 25)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else if (accuracy >= 24 && Globals::Shots >= 2)
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Chest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					PredictBypass();
				}
				else
				{
					PredictBypass();
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Neck);
					HitBoxesToScan.push_back((int)CSGOHitboxID::Head);
					HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
					HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
					HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
			}
			PredictBypass();
			break;
			PredictBypass();
		}
		case 5:
		{
			PredictBypass();
			HitBoxesToScan.clear();
			HitBoxesToScan.push_back((int)CSGOHitboxID::NeckLower);
			HitBoxesToScan.push_back((int)CSGOHitboxID::RightFoot);
			HitBoxesToScan.push_back((int)CSGOHitboxID::LeftFoot);
			HitBoxesToScan.push_back((int)CSGOHitboxID::Pelvis);
			HitBoxesToScan.push_back((int)CSGOHitboxID::Stomach);
			HitBoxesToScan.push_back((int)CSGOHitboxID::LeftLowerArm);
			HitBoxesToScan.push_back((int)CSGOHitboxID::RightUpperArm);
			HitBoxesToScan.push_back((int)CSGOHitboxID::LeftThigh);
			HitBoxesToScan.push_back((int)CSGOHitboxID::RightThigh);
			HitBoxesToScan.push_back((int)CSGOHitboxID::LeftShin);
			HitBoxesToScan.push_back((int)CSGOHitboxID::RightShin);
			HitBoxesToScan.push_back((int)CSGOHitboxID::UpperChest);
			HitBoxesToScan.push_back((int)CSGOHitboxID::RightHand);
			HitBoxesToScan.push_back((int)CSGOHitboxID::LeftHand);
			PredictBypass();
		}
		PredictBypass();
		break;
		PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();

#pragma endregion Get the list of shit to scan
	for (auto HitBoxID : HitBoxesToScan)
	{
		PredictBypass();
		if (AWall)
		{
			PredictBypass();
			Vector Point = GetHitboxPosition(pEntity, HitBoxID);
			float Damage = 0.f;
			Color c = Color(255, 255, 255, 255);
			if (CanHit(Point, &Damage))
			{
				c = Color(0, 255, 0, 255);
				if (Damage >= options.Rage.autowalldmg)
				{
					return HitBoxID;
					PredictBypass();
				}
				PredictBypass();
			}
			PredictBypass();
		}
		else
		{
			if (GameUtils::IsVisible(hackManager.pLocal(), pEntity, HitBoxID))
				return HitBoxID;
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	return -1;
	PredictBypass();
}

void gshdgkjshgsuighsg::DoNoRecoil(CUserCmd *pCmd)
{
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	if (pLocal)
	{
		PredictBypass();
		Vector AimPunch = pLocal->localPlayerExclusive()->GetAimPunchAngle();
		if (AimPunch.Length2D() > 0 && AimPunch.Length2D() < 150)
		{
			PredictBypass();
			pCmd->viewangles -= AimPunch * 2;
			GameUtils::NormaliseViewAngle(pCmd->viewangles);
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
}







CAntiSpread* NoSpread = new CAntiSpread;


void CAntiSpread::CalcServer(Vector vSpreadVec, Vector ViewIn, Vector &vecSpreadDir)
{
	Vector vecViewForward, vecViewRight, vecViewUp;

	angvec(ViewIn, vecViewForward, vecViewRight, vecViewUp);

	vecSpreadDir = vecViewForward + vecViewRight * vSpreadVec.x + vecViewUp * vSpreadVec.y;
}

void CAntiSpread::antispread(CUserCmd* pCmd)
{
	Vector vecForward, vecRight, vecDir, vecUp, vecAntiDir;
	float flSpread, flInaccuracy;
	Vector qAntiSpread;

//	glb::mainwep->UpdateAccuracyPenalty();

	flSpread = glb::mainwep->GetSpread();

	flInaccuracy = glb::mainwep->GetInaccuracy();
	Globals::UserCmd->random_seed = MD5_PseudoRandom(Globals::UserCmd->command_number) & 0x7FFFFFFF;
	RandomSeed((Globals::UserCmd->random_seed & 0xFF) + 1);


	float fRand1 = RandFloat(0.f, 1.f);
	float fRandPi1 = RandFloat(0.f, 2.f * (float)M_PI);
	float fRand2 = RandFloat(0.f, 1.f);
	float fRandPi2 = RandFloat(0.f, 2.f * (float)M_PI);

	float m_flRecoilIndex = glb::mainwep->GetFloatRecoilIndex();


	if (glb::mainwep->WeaponID() == 64)
	{
		if (Globals::UserCmd->buttons & IN_ATTACK2)
		{
			fRand1 = 1.f - fRand1 * fRand1;
			fRand2 = 1.f - fRand2 * fRand2;
		}
	}
	else if (glb::mainwep->WeaponID() == NEGEV && m_flRecoilIndex < 3.f)
	{
		for (int i = 3; i > m_flRecoilIndex; --i)
		{
			fRand1 *= fRand1;
			fRand2 *= fRand2;
		}

		fRand1 = 1.f - fRand1;
		fRand2 = 1.f - fRand2;
	}

	float fRandInaccuracy = fRand1 * glb::mainwep->GetInaccuracy();
	float fRandSpread = fRand2 * glb::mainwep->GetSpread();

	float fSpreadX = cos(fRandPi1) * fRandInaccuracy + cos(fRandPi2) * fRandSpread;
	float fSpreadY = sin(fRandPi1) * fRandInaccuracy + sin(fRandPi2) * fRandSpread;


	pCmd->viewangles.x += RAD2DEG(atan(sqrt(fSpreadX * fSpreadX + fSpreadY * fSpreadY)));
	pCmd->viewangles.z = RAD2DEG(atan2(fSpreadX, fSpreadY));
}

void gshdgkjshgsuighsg::ns1(CUserCmd *pCmd, IClientEntity* LocalPlayer)
{
	PredictBypass();
	float recoil_value = 2;
//	glb::mainwep->UpdateAccuracyPenalty();
	QAngle punch = LocalPlayer->GetPunchAngle();

	if (Interfaces::CVar->FindVar(XorStr("weapon_recoil_scale")))
	{
		ConVar* recoil_value = Interfaces::CVar->FindVar(XorStr("weapon_recoil_scale"));
		recoil_value->SetValue("2");
		PredictBypass();
	}
	Globals::UserCmd->viewangles -= punch * recoil_value;
	PredictBypass();
}

void gshdgkjshgsuighsg::nospread(CUserCmd *pCmd)
{
	PredictBypass();
	IClientEntity* LocalPlayer= hackManager.pLocal();
	if (options.Rage.AcurracyControl == 2 || options.Rage.AcurracyControl == 3)
	{
		PredictBypass();
		ns1(pCmd, LocalPlayer);
		Globals::UserCmd->viewangles = NoSpread->SpreadFactor(Globals::UserCmd->random_seed);
		PredictBypass();
	}
	PredictBypass();

	

}

// 	float m_fValue;
void gshdgkjshgsuighsg::aimAtPlayer(CUserCmd *pCmd)
{
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	PredictBypass();
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());
	PredictBypass();
	if (!pLocal || !pWeapon)
		return;
	PredictBypass();
	Vector eye_position = pLocal->GetEyePosition();
	PredictBypass();
	float best_dist = pWeapon->GetCSWpnData()->m_flRange;
	PredictBypass();
	IClientEntity* target = nullptr;
	PredictBypass();
	for (int i = 0; i < Interfaces::Engine->GetMaxClients(); i++)
	{
		PredictBypass();
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		if (TargetMeetsRequirements(pEntity))
		{
			PredictBypass();
			if (Globals::TargetID != -1)
				target = Interfaces::EntList->GetClientEntity(Globals::TargetID);
			else
				target = pEntity;
			PredictBypass();
			Vector target_position = target->GetEyePosition();
			PredictBypass();
			float temp_dist = eye_position.DistTo(target_position);
			PredictBypass();
			if (best_dist > temp_dist)
			{
				best_dist = temp_dist;
				CalcAngle(eye_position, target_position, pCmd->viewangles);
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
}


float GetBestHeadAngle(float yaw)
{
	float Back, Right, Left;
	IClientEntity* pLocal = hackManager.pLocal();
	Vector src3D, dst3D, forward, right, up, src, dst;
	trace_t tr;
	Ray_t ray, ray2, ray3, ray4, ray5;
	CTraceFilter filter;
	IEngineTrace trace;
	QAngle angles;
	QAngle engineViewAngles;
	Interfaces::Engine->GetViewAngles(angles);

	engineViewAngles.x = 0;

	Math::AngleVectors(engineViewAngles, &forward, &right, &up);

	filter.pSkip = pLocal;
	src3D = pLocal->GetEyeAngles();
	dst3D = src3D + (forward * 384);

	ray.Init(src3D, dst3D);

	Interfaces::Trace->TraceRay(ray, MASK_SHOT, &filter, &tr);

	Back = (tr.endpos - tr.startpos).Length();

	ray2.Init(src3D + right * 35, dst3D + right * 35);

	Interfaces::Trace->TraceRay(ray2, MASK_SHOT, &filter, &tr);

	Right = (tr.endpos - tr.startpos).Length();

	ray3.Init(src3D - right * 35, dst3D - right * 35);

	Interfaces::Trace->TraceRay(ray3, MASK_SHOT, &filter, &tr);

	Left = (tr.endpos - tr.startpos).Length();

	if (Left > Right)
	{
		return (yaw - 90);
	}
	else if (Right > Left)
	{
		return (yaw + 90);
	}
	else if (Back > Right || Back > Left)
	{
		return (yaw - 180);
	}
	return 0;
}


bool ShouldPredict()
{
	INetChannelInfo* nci = Interfaces::Engine->GetNetChannelInfo();

	IClientEntity* pLocal = hackManager.pLocal();
	float server_time = Interfaces::Globals->curtime + nci->GetLatency(FLOW_OUTGOING);

	float PredictedTime = 0.f;
	static bool initialized;

	bool will_update = false;

	if (!initialized && pLocal->IsMoving())
	{
		initialized = true;
		PredictedTime = server_time + 0.22f;
	}
	else if (pLocal->IsMoving())
	{
		initialized = false;
	}

	if (server_time >= (PredictedTime) && pLocal->GetFlags() & FL_ONGROUND)
	{
		PredictedTime = server_time + 1.1f;
		will_update = true;
	}
	return will_update;
}


bool gshdgkjshgsuighsg::AimAtPoint(IClientEntity* pLocal, Vector point, CUserCmd *pCmd, bool &bSendPacket)
{
	PredictBypass();
	bool ReturnValue = false;
	PredictBypass();
	if (point.Length() == 0) return ReturnValue;
	PredictBypass();
	Vector angles;
	Vector src = pLocal->GetOrigin() + pLocal->GetViewOffset();
	PredictBypass();
	CalcAngle(src, point, angles);
	GameUtils::NormaliseViewAngle(angles);
	PredictBypass();
	if (angles[0] != angles[0] || angles[1] != angles[1])
	{
		return ReturnValue;
		PredictBypass();
	}
	PredictBypass();
	IsLocked = true;

	Vector ViewOffset = pLocal->GetOrigin() + pLocal->GetViewOffset();
	if (!IsAimStepping)
		LastAimstepAngle = LastAngle; 
	PredictBypass();
	float fovLeft = FovToPlayer(ViewOffset, LastAimstepAngle, Interfaces::EntList->GetClientEntity(TargetID), 0);
	PredictBypass();
	if (fovLeft > 25.0f && Menu::Window.RageBotTab.AimbotAimStep.GetState())
	{
		PredictBypass();
		Vector AddAngs = angles - LastAimstepAngle;
		Normalize(AddAngs, AddAngs);
		AddAngs *= 25;
		LastAimstepAngle += AddAngs;
		GameUtils::NormaliseViewAngle(LastAimstepAngle);
		angles = LastAimstepAngle;
		PredictBypass();
	}
	else
	{
		PredictBypass();
		ReturnValue = true;
		PredictBypass();
	}
	PredictBypass();
	if (options.Rage.SilentTYPE)
	{
		pCmd->viewangles = angles;
		PredictBypass();

	}

	if (!options.Rage.SilentTYPE)
	{
		PredictBypass();
		Interfaces::Engine->SetViewAngles(angles);
		PredictBypass();
	}
	PredictBypass();
	return ReturnValue;
	PredictBypass();
}

namespace AntiAims
{


	void FastSpin(CUserCmd *pCmd)
	{
		static int y2 = -179;
		int spinBotSpeedFast = 75;

		y2 += spinBotSpeedFast;

		if (y2 >= 179)
			y2 = -179;

		pCmd->viewangles.y = y2;
	}



	void FastSpint(CUserCmd *pCmd)
	{
		int r1 = rand() % 100;
		int r2 = rand() % 1000;

		static bool dir;
		static float current_y = pCmd->viewangles.y;

		if (r1 == 1) dir = !dir;

		if (dir)
			current_y += 15 + rand() % 10;
		else
			current_y -= 15 + rand() % 10;

		pCmd->viewangles.y = current_y;

		if (r1 == r2)
			pCmd->viewangles.y += r1;
	}

	void Up(CUserCmd *pCmd)
	{
		pCmd->viewangles.x = -89.0f;
	}

	void Zero(CUserCmd *pCmd)
	{
		pCmd->viewangles.x = 0.f;
	}

	void fforward(CUserCmd *pCmd, bool &bSendPacket)
	{
		pCmd->viewangles.y -= 0.f;
	}

	void StaticRealYaw(CUserCmd *pCmd, bool &bSendPacket) {
		bSendPacket = false;
		pCmd->viewangles.y += 90;
	}



	void freak(IClientEntity* pLocal, CUserCmd *pCmd, bool &bSendPacket)
	{
		if (pLocal->GetFlags()  & pLocal->GetVelocity().Length() <= 0.f & FL_ONGROUND)
		{
			QAngle angles;
			Interfaces::Engine->GetViewAngles(angles);

			float BestHeadPosition = GetBestHeadAngle(angles.y);

			float LowerbodyDelta = 160;

			if (bSendPacket)
			{
				pCmd->viewangles.y = BestHeadPosition + LowerbodyDelta + Math::RandomFloat2(-40.f, 40.f);
			}
			else
			{
				if (ShouldPredict())
					pCmd->viewangles.y = BestHeadPosition + LowerbodyDelta;
				else
					pCmd->viewangles.y = BestHeadPosition;
			}
		}



	}


	void freakmove(IClientEntity* pLocal, CUserCmd *pCmd, bool &bSendPacket)
	{
		if (pLocal->GetFlags() && pLocal->GetVelocity().Length() <= 245.f & FL_ONGROUND)
		{
			QAngle angles;
			Interfaces::Engine->GetViewAngles(angles);

			float BestHeadPosition = GetBestHeadAngle(angles.y);

			float LowerbodyDelta = 160;

			if (bSendPacket)
			{
				pCmd->viewangles.y = BestHeadPosition + LowerbodyDelta + Math::RandomFloat2(-40.f, 40.f);
			}
			else
			{
				if (ShouldPredict())
					pCmd->viewangles.y = BestHeadPosition + LowerbodyDelta;
				else
					pCmd->viewangles.y = BestHeadPosition;
			}
		}
		else
			pCmd->viewangles.y += 180;


	}
	void custombuildPitch(IClientEntity* pLocal, CUserCmd *pCmd, bool &bSendPacket)
	{
		static bool quickmathematics = false;
		float custombase = (Menu::Window.aabuild.pitchbase.GetValue());

		float pitch_jiiter_from = (Menu::Window.aabuild.pitch_jitter_from.GetValue());
		float pitch_jiiter_to = (Menu::Window.aabuild.pitch_jitter_to.GetValue());

		float pitch_safe_fake = (Menu::Window.aabuild.pitch_safe_fake.GetValue());
		float pitch_safe_real = (Menu::Window.aabuild.pitch_safe_real.GetValue());

		float pitch_unsafe_fake = (Menu::Window.aabuild.pitch_unsafe_fake.GetValue());
		float pitch_unsafe_real = (Menu::Window.aabuild.pitch_unsafe_real.GetValue());


		if (Menu::Window.aabuild.pitchpick.GetIndex() == 0)
		{
			pCmd->viewangles.x = custombase;
		}
		else if (Menu::Window.aabuild.pitchpick.GetIndex() == 1)
		{

			if (quickmathematics)
			{
				pCmd->viewangles.x = pitch_jiiter_from;
			}
			else
			{
				pCmd->viewangles.x = pitch_jiiter_to;
			}
			quickmathematics = !quickmathematics;
		}
		else if (Menu::Window.aabuild.pitchpick.GetIndex() == 2)
		{
			if (bSendPacket)
				pitch_safe_fake;
			else
				pitch_safe_real;
		}
		else
		{
			if (bSendPacket)
				pitch_unsafe_fake;
			else
				pitch_unsafe_real;
		}
	}


	void custombuildYaw(IClientEntity* pLocal, CUserCmd *pCmd, bool &bSendPacket)
	{
		float value;
		float rndy = rand() % 60;
		static bool switch3 = false;

		if (rndy < 15)
			rndy = 15;


		float custombase = (Menu::Window.aabuild.yawbase.GetValue()); // Starting point

		float switch1 = (Menu::Window.aabuild.yaw_switch_from.GetValue());
		float switch2 = (Menu::Window.aabuild.yaw_switch_to.GetValue());

		float jit1 = (Menu::Window.aabuild.yaw_jitter_from.GetValue());
		float jit2 = (Menu::Window.aabuild.yaw_jitter_to.GetValue());

		float spinspeed = (Menu::Window.aabuild.spinspeed.GetValue());

		//--------------------lby-trash-idk-lmao--------------------//
		int flip = (int)floorf(Interfaces::Globals->curtime / 1.1) % 2;
		static bool bFlipYaw;
		static bool wilupdate;
		float flInterval = Interfaces::Globals->interval_per_tick;
		float flTickcount = pCmd->tick_count;
		float flTime = flInterval * flTickcount;
		if (std::fmod(flTime, 1) == 0.f)
			bFlipYaw = !bFlipYaw;
		static float LastLBYUpdateTime = 0;

		float server_time = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;
		//--------------------lby-trash-idk-lmao--------------------//


		if (Menu::Window.aabuild.yawpick.GetIndex() == 0)
		{
			pCmd->viewangles.y -= custombase;
		}
		else if (Menu::Window.aabuild.yawpick.GetIndex() == 1) // jitter AA
		{

			if (switch3)
				pCmd->viewangles.y = jit1;
			else
				pCmd->viewangles.y = jit2;
			switch3 = !switch3;

		}
		else if (Menu::Window.aabuild.yawpick.GetIndex() == 2) // Switch AA
		{

			if (Menu::Window.aabuild.yaw_add_jitter.GetState() == false) // if we do not add jitter
			{
				if (flip)
				{
					pCmd->viewangles.y += bFlipYaw ? switch1 : switch2;

				}
				else
				{
					pCmd->viewangles.y -= hackManager.pLocal()->GetLowerBodyYaw() + bFlipYaw ? switch1 : switch2;
				}
			}
			else // if we add jitter
			{
				if (flip)
				{
					pCmd->viewangles.y += bFlipYaw ? switch1 + rndy : switch2 - rndy;

				}
				else
				{
					pCmd->viewangles.y -= hackManager.pLocal()->GetLowerBodyYaw() + bFlipYaw ? switch1 + rndy : switch2 - rndy;
				}
			}

		}
		else
		{
			static int y2 = -170;
			int spinBotSpeedFast = spinspeed;

			y2 += spinBotSpeedFast;

			if (y2 >= 170)
				y2 = -170;

			pCmd->viewangles.y = y2;

		}

	}
	void custombuildFake(IClientEntity* pLocal, CUserCmd *pCmd, bool &bSendPacket)
	{
		float value;
		float rndy = rand() % 60;
		static bool switch3 = false;

		if (rndy < 15)
			rndy = 15;


		float custombase = (Menu::Window.aabuild.fyawbase.GetValue()); // Starting point

		float switch1 = (Menu::Window.aabuild.fyaw_switch_from.GetValue());
		float switch2 = (Menu::Window.aabuild.fyaw_switch_to.GetValue());

		float jit1 = (Menu::Window.aabuild.fyaw_jitter_from.GetValue());
		float jit2 = (Menu::Window.aabuild.fyaw_jitter_to.GetValue());

		float spinspeed = (Menu::Window.aabuild.fspinspeed.GetValue());

		//--------------------lby-trash-idk-lmao--------------------//
		int flip = (int)floorf(Interfaces::Globals->curtime / 1.1) % 2;
		static bool bFlipYaw;
		static bool wilupdate;
		float flInterval = Interfaces::Globals->interval_per_tick;
		float flTickcount = pCmd->tick_count;
		float flTime = flInterval * flTickcount;
		if (std::fmod(flTime, 1) == 0.f)
			bFlipYaw = !bFlipYaw;
		static float LastLBYUpdateTime = 0;

		float server_time = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;
		//--------------------lby-trash-idk-lmao--------------------//


		if (Menu::Window.aabuild.fyawpick.GetIndex() == 0)
		{
			if (bSendPacket)
				pCmd->viewangles.y -= custombase;
		}
		else if (Menu::Window.aabuild.fyawpick.GetIndex() == 1) // jitter AA
		{
			if (bSendPacket)
			{
				if (switch3)
					pCmd->viewangles.y = jit1;
				else
					pCmd->viewangles.y = jit2;
				switch3 = !switch3;
			}

		}
		else if (Menu::Window.aabuild.fyawpick.GetIndex() == 2) // Switch AA
		{

			if (Menu::Window.aabuild.yaw_add_jitter.GetState() == false) // if we do not add jitter
			{
				if (bSendPacket)
				{
					if (flip)
					{
						pCmd->viewangles.y += bFlipYaw ? switch1 : switch2;

					}
					else
					{
						pCmd->viewangles.y -= hackManager.pLocal()->GetLowerBodyYaw() + bFlipYaw ? switch1 : switch2;
					}
				}
			}
			else // if we add jitter
			{
				if (bSendPacket)
				{
					if (flip)
					{
						pCmd->viewangles.y += bFlipYaw ? switch1 + rndy : switch2 + rndy;

					}
					else
					{
						pCmd->viewangles.y -= hackManager.pLocal()->GetLowerBodyYaw() + bFlipYaw ? switch1 - rndy : switch2 - rndy;
					}
				}
			}

		}
		else
		{
			if (bSendPacket)
			{
				static int y2 = -170;
				int spinBotSpeedFast = spinspeed;

				y2 += spinBotSpeedFast;

				if (y2 >= 170)
					y2 = -170;

				pCmd->viewangles.y = y2;
			}

		}
	}






	void LBYSide(CUserCmd *pCmd, bool &bSendPacket)
	{
		int i = 0; i < Interfaces::EntList->GetHighestEntityIndex(); ++i;
		IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
		IClientEntity *pLocal = Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer());

		static bool isMoving;
		float PlayerIsMoving = abs(pLocal->GetVelocity().Length());
		if (PlayerIsMoving > 0.1) isMoving = true;
		else if (PlayerIsMoving <= 0.1) isMoving = false;
		int meme = rand() % 75;
		if (meme < 17)
			meme = 17;
		int flip = (int)floorf(Interfaces::Globals->curtime / 1.1) % 2;
		static bool bFlipYaw;
		float flInterval = Interfaces::Globals->interval_per_tick;
		float flTickcount = pCmd->tick_count;
		float flTime = flInterval * flTickcount;
		if (std::fmod(flTime, 1) == 0.f)
			bFlipYaw = !bFlipYaw;

		if (PlayerIsMoving <= 0.4)
		{
			if (bSendPacket)
			{
				pCmd->viewangles.y += bFlipYaw ? (90.f + meme) : (-90 - meme);
			}
			else
			{
				if (flip)
				{
					pCmd->viewangles.y += bFlipYaw ? (90.f + meme) : (-90 - meme);

				}
				else
				{
					pCmd->viewangles.y -= hackManager.pLocal()->GetLowerBodyYaw() + bFlipYaw ? (90.f + meme) : (-90 - meme);
				}
			}
		}
		else
		{
			if (bSendPacket)
			{
				pCmd->viewangles.y += 5.f - meme;
			}
			else
			{
				pCmd->viewangles.y += 160.f - meme;
			}
		}
	}


	void SideJitter(CUserCmd *pCmd)
	{
		static bool Fast2 = false;
		if (Fast2)
		{
			pCmd->viewangles.y += 75;
		}
		else
		{
			pCmd->viewangles.y += 105;
		}
		Fast2 = !Fast2;
	}

	void SlowSpin(CUserCmd *pCmd)
	{
		int r1 = rand() % 100;
		int r2 = rand() % 1000;

		static bool dir;
		static float current_y = pCmd->viewangles.y;

		if (r1 == 1) dir = !dir;

		if (dir)
			current_y += 4 + rand() % 10;
		else
			current_y -= 4 + rand() % 10;

		pCmd->viewangles.y = current_y;

		if (r1 == r2)
			pCmd->viewangles.y += r1;
	}
}

void CorrectMovement(Vector old_angles, CUserCmd* cmd, float old_forwardmove, float old_sidemove)
{
	float delta_view, first_function, second_function;

	if (old_angles.y < 0.f) first_function = 360.0f + old_angles.y;
	else first_function = old_angles.y;
	if (cmd->viewangles.y < 0.0f) second_function = 360.0f + cmd->viewangles.y;
	else second_function = cmd->viewangles.y;

	if (second_function < first_function) delta_view = abs(second_function - first_function);
	else delta_view = 360.0f - abs(first_function - second_function);

	delta_view = 360.0f - delta_view;

	cmd->forwardmove = cos(DEG2RAD(delta_view)) * old_forwardmove + cos(DEG2RAD(delta_view + 90.f)) * old_sidemove;
	cmd->sidemove = sin(DEG2RAD(delta_view)) * old_forwardmove + sin(DEG2RAD(delta_view + 90.f)) * old_sidemove;
}

float GetLatency()
{
	INetChannelInfo *nci = Interfaces::Engine->GetNetChannelInfo();
	if (nci)
	{

		float Latency = nci->GetAvgLatency(FLOW_OUTGOING) + nci->GetAvgLatency(FLOW_INCOMING);
		return Latency;
	}
	else
	{

		return 0.0f;
	}
}
float GetOutgoingLatency()
{
	INetChannelInfo *nci = Interfaces::Engine->GetNetChannelInfo();
	if (nci)
	{

		float OutgoingLatency = nci->GetAvgLatency(FLOW_OUTGOING);
		return OutgoingLatency;
	}
	else
	{

		return 0.0f;
	}
}
float GetIncomingLatency()
{
	INetChannelInfo *nci = Interfaces::Engine->GetNetChannelInfo();
	if (nci)
	{
		float IncomingLatency = nci->GetAvgLatency(FLOW_INCOMING);
		return IncomingLatency;
	}
	else
	{

		return 0.0f;
	}
}

float OldLBY;
float LBYBreakerTimer;
float LastLBYUpdateTime;
bool bSwitch;

float CurrentVelocity(IClientEntity* LocalPlayer)
{
	int vel = LocalPlayer->GetVelocity().Length2D();
	return vel;
}
bool NextLBYUpdate()
{
	IClientEntity* LocalPlayer = hackManager.pLocal();

	float flServerTime = (float)(LocalPlayer->GetTickBase()  * Interfaces::Globals->interval_per_tick);


	if (OldLBY != LocalPlayer->GetLowerBodyYaw())
	{

		LBYBreakerTimer++;
		OldLBY = LocalPlayer->GetLowerBodyYaw();
		bSwitch = !bSwitch;
		LastLBYUpdateTime = flServerTime;
	}

	if (CurrentVelocity(LocalPlayer) > 1)
	{
		LastLBYUpdateTime = flServerTime;
		return false;
	}

	if ((LastLBYUpdateTime + 1 - (GetLatency() * 2) < flServerTime) && (LocalPlayer->GetFlags() & FL_ONGROUND))
	{
		if (LastLBYUpdateTime + 1.09 - (GetLatency() * 2) < flServerTime)
		{
			LastLBYUpdateTime += 1.1;
		}
		return true;
	}
	return false;
}

bool NextMovingLBYUpdate()
{
	IClientEntity* LocalPlayer = hackManager.pLocal();

	float flServerTime = (float)(LocalPlayer->GetTickBase()  * Interfaces::Globals->interval_per_tick);


	if (OldLBY != LocalPlayer->GetLowerBodyYaw())
	{

		LBYBreakerTimer++;
		OldLBY = LocalPlayer->GetLowerBodyYaw();
		bSwitch = !bSwitch;
		LastLBYUpdateTime = flServerTime;
	}

	if (CurrentVelocity(LocalPlayer) > 1)
	{
		LastLBYUpdateTime = flServerTime;
		return false;
	}

	if ((LastLBYUpdateTime + 1 - (GetLatency() * 2) < flServerTime) && (LocalPlayer->GetFlags() & FL_ONGROUND))
	{
		if (LastLBYUpdateTime + 0.2171 - (GetLatency() * 2) < flServerTime)
		{
			LastLBYUpdateTime += 0.21712;
		}
		return true;
	}
	return false;
}




#define RandomInt(min, max) (rand() % (max - min + 1) + min)
void DoLBYBreak(CUserCmd * pCmd, IClientEntity* pLocal, bool& bSendPacket)
{
	if (!bSendPacket)
	{
		pCmd->viewangles.y -= 90;
	}
	else
	{
		pCmd->viewangles.y += 90;
	}
}

void fakelby(CUserCmd* cmd, bool &bSendPacket)
{
	
	
	IClientEntity* LocalPlayer = hackManager.pLocal();
	float value = LocalPlayer->GetLowerBodyYaw();
	float flServerTime = (float)(LocalPlayer->GetTickBase()  * Interfaces::Globals->interval_per_tick);

	if ((LastLBYUpdateTime + 1 - (GetLatency() * 2) < flServerTime))
	{
		
		if (bSendPacket)
		{
			cmd->viewangles.y = value - 119 + rand() % 90;
		}
	
	
	}
	else
	{
		if (bSendPacket)
		{
			cmd->viewangles.y = -80.00 + rand() % 150;
		}
	}
	
}

void DoLBYBreakReal(CUserCmd * pCmd, IClientEntity* pLocal, bool& bSendPacket)
{
	if (!bSendPacket)
	{
		pCmd->viewangles.y += 90;
	}
	else
	{
		pCmd->viewangles.y -= 90;
	}
}


static bool peja;
static bool switchbrak;
static bool safdsfs;

void Dynamic(CUserCmd *pCmd, bool &bSendPacket)
{
	IClientEntity* pLocal = hackManager.pLocal();
	static int skeet = 179;
	int SpinSpeed = 500;
	static int ChokedPackets = -1;
	ChokedPackets++;
	skeet += SpinSpeed;

	if (pCmd->command_number % 20)
	{
		if (skeet >= pLocal->GetLowerBodyYaw() + 179 - rand() % 30);
		skeet = pLocal->GetLowerBodyYaw() - 0;
		ChokedPackets = -1;
	}
	if (pCmd->command_number % 30)
	{
		float CalculatedCurTime = (Interfaces::Globals->curtime * 1000.0);
		pCmd->viewangles.y = CalculatedCurTime;
		ChokedPackets = -1;
	}

	pCmd->viewangles.y = skeet;
}


void jolt(CUserCmd *pCmd, bool &bSendPacket)
{
	// (LastLBYUpdateTime + 1 - (GetLatency() * 2) < flServerTime)
	IClientEntity* pLocal = hackManager.pLocal();
	float value = pLocal->GetLowerBodyYaw();
	float flServerTime = (float)(pLocal->GetTickBase()  * Interfaces::Globals->interval_per_tick);
	float moving = abs(pLocal->GetVelocity().Length());
	if (bSendPacket)
	{
		if (moving > 30)
		{
			pCmd->viewangles.y = value + (101 + rand() % 20);
		}
		else
		{
			pCmd->viewangles.y = (value - 90) - rand() % 110;
		}
	}

}



void Keybased(CUserCmd *pCmd, bool &bSendPacket)
{
	IClientEntity* pLocal = hackManager.pLocal();
	
	static bool dir = false;
	float side = pCmd->viewangles.y - 90;
	float server_time = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;
	if (GetAsyncKeyState(VK_LEFT)) dir = false; 
	else if (GetAsyncKeyState(VK_RIGHT)) dir = true;

	if (dir && pLocal->GetVelocity().Length2D() >= 0)
	{
		pCmd->viewangles.y -= 90;
	}
	else if (!dir && pLocal->GetVelocity().Length2D() >= 0)
	{
		pCmd->viewangles.y += 90;
	}

	// literally tapping unityhacks omg media
}


void LowerBodyFake(CUserCmd *pCmd, bool &bSendPacket)
{

	static bool wilupdate;
	static float LastLBYUpdateTime = 0;
	IClientEntity* pLocal = hackManager.pLocal();
	float server_time = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;

	static int ChokedPackets = -1;
	ChokedPackets++;
	if (ChokedPackets < 1)
	{
		

		if (server_time >= LastLBYUpdateTime)
		{
			LastLBYUpdateTime = server_time + 1.009f;
			wilupdate = true;
			pCmd->viewangles.y += hackManager.pLocal()->GetLowerBodyYaw() - RandomInt(80, 150, -90, -160);
		}
		else
		{
			wilupdate = false;
			pCmd->viewangles.y += hackManager.pLocal()->GetLowerBodyYaw() + RandomInt(-160, -90, 90, 170);
		}
		bSendPacket = true;
	}

}
static bool dir = false;
void DoRealAA(CUserCmd* pCmd, IClientEntity* pLocal, bool& bSendPacket)
{


	float memeantilby = options.HvH.DeltaMove;


	int y2 = 90;
	int spinspeed = 65;


	if (GetAsyncKeyState(options.Rage.rightkey)) dir = false;
	else if (GetAsyncKeyState(options.Rage.leftkey)) dir = true;
	float server_time = pLocal->GetTickBase() * Interfaces::Globals->interval_per_tick;

	bool wilupdate;
	static bool switch2;
	Vector oldAngle = pCmd->viewangles;
	float oldForward = pCmd->forwardmove;
	float oldSideMove = pCmd->sidemove;

	if (!options.Rage.enableaa)
		return;


	float rando = rand() % 1000;



	if (pLocal->GetFlags() && pLocal->GetVelocity().Length() > 2)
	{


		switch (options.Rage.moveyaa)
		{
		case 1:
			//backwards
			pCmd->viewangles.y -= 180;
			break;
		case 2:
			if (switch2)
				pCmd->viewangles.y -= (160 - rand() % 25);
			else
				pCmd->viewangles.y += (160 + rand() % 25);
			switch2 = !switch2;
			break;
		case 3:
		{

			if (rando < 499)
			{
				if (switch2)
					pCmd->viewangles.y = 180;
				else
					pCmd->viewangles.y = 30;
				switch2 = !switch2;
			}
			else if (rando > 500)
			{
				if (switch2)
					pCmd->viewangles.y = 90;
				else
					pCmd->viewangles.y = -90;
				switch2 = !switch2;
			}
			else
				pCmd->viewangles.y = 80;

		}
		break;
		case 4:
		{
			pCmd->viewangles.y = (145 + rand() % 70);
		}
		case 5:
			pCmd->viewangles.y = pLocal->GetLowerBodyYaw() - 179 + rand() % 160;
			break;
		case 6:
		{
			if (NextLBYUpdate())
			{
				pCmd->viewangles.y -= 120;
			}
			else
			{
				pCmd->viewangles.y += 120;
			}
		}
		break;
		case 7:
		{
			if (dir)
			{
				pCmd->viewangles.y += 90;
			}
			else if (!dir)
			{
				pCmd->viewangles.y -= 90;
			}
		}
		break;
		case 8:
		{
			y2 += spinspeed;

			if (y2 >= 179)
				y2 = -179;

			pCmd->viewangles.y = y2;


		}
		break;
		case 9:
		{

			if (pLocal->IsMoving())
			{
				y2 += spinspeed;

				if (y2 >= -120)
					y2 = 120;

				pCmd->viewangles.y = y2;
			}
			else
			{
				if (switch2)
					pCmd->viewangles.y -= 150;
				else
					pCmd->viewangles.y += 150;
				switch2 = !switch2;
			}

		}
		break;
		}
	}
	else
	{
		switch (options.Rage.realyaa)
		{
		case 1:
			//backwards
			pCmd->viewangles.y -= 180;
			break;
		case 2:
			if (switch2)
				pCmd->viewangles.y += (160 + rand() % 25);
			else
				pCmd->viewangles.y -= (160 - rand() % 25);
			switch2 = !switch2;
			break;
		case 3:
		{

			if (rando < 499)
			{
				if (switch2)
					pCmd->viewangles.y -= 170;
				else
					pCmd->viewangles.y += 30;
				switch2 = !switch2;
			}
			else if (rando > 500)
			{
				if (switch2)
					pCmd->viewangles.y += 90;
				else
					pCmd->viewangles.y -= 90;
				switch2 = !switch2;
			}
			else
				pCmd->viewangles.y -= 80;

		}
		break;
		case 4:
		{
			pCmd->viewangles.y -= (145 + rand() % 70);
		}
		case 5:
			pCmd->viewangles.y -= (pLocal->GetLowerBodyYaw() - 179) + rand() % 160;
			break;
		case 6:
		{
			if (server_time >= LastLBYUpdateTime)
			{
				LastLBYUpdateTime = server_time + 1.090f;
				wilupdate = true;
				pCmd->viewangles.y += hackManager.pLocal()->GetLowerBodyYaw() - RandomInt(80, 150, -90, -160);
			}
			else
			{
				wilupdate = false;
				pCmd->viewangles.y += hackManager.pLocal()->GetLowerBodyYaw() + RandomInt(-160, -90, 90, 170);
			}
			bSendPacket = true;

		}
		break;
		case 7:
		{
			if (dir)
			{
				pCmd->viewangles.y += 90;
			}
			else if (!dir)
			{
				pCmd->viewangles.y -= 90;
			}
		}
		break;
		case 8:
		{
			y2 += spinspeed;

			if (y2 >= 179)
				y2 = -179;

			pCmd->viewangles.y = y2;


		}
		break;
		case 9:
		{

			if (pLocal->IsMoving())
			{
				y2 += spinspeed;

				if (y2 >= -119)
					y2 = 119;

				pCmd->viewangles.y = y2;
			}
			else
			{
				if (switch2)
					pCmd->viewangles.y += 150;
				else
					pCmd->viewangles.y -= 150;
				switch2 = !switch2;
			}

		}
		break;
		}
	}





}




void DoFakeAA(CUserCmd* pCmd, bool& bSendPacket, IClientEntity* pLocal)
{
	static int ChokedPackets = -1;
	ChokedPackets++;
	static bool switch2;
	Vector oldAngle = pCmd->viewangles;
	float oldForward = pCmd->forwardmove;
	float oldSideMove = pCmd->sidemove;
	IClientEntity* pEntity;
	int flip = (int)floorf(Interfaces::Globals->curtime / 1.1) % 2;
	static bool bFlipYaw;
	static bool wilupdate;



	int y2 = -90;
	int spinspeed = 66;


	float memeantilby = options.HvH.DeltaMove;



	if (!options.Rage.enableaa)
		return;


	switch (options.Rage.fakeyyaa)
	{
	case 0: { /* Muslims suck */ } break;
	case 1:
		//backwards
		pCmd->viewangles.y -= 180.000000;
		break;
	case 2:
	{
		if (switch2)
		{
			pCmd->viewangles.y = -150;
			ChokedPackets = 1;

		}
		else
		{
			pCmd->viewangles.y = 150;
			ChokedPackets = 1;
		}
		switch2 = !switch2;
	}
	break;
	case 3:
	{
		int rando = rand() % 750;
		if (rando < 375)
		{
			if (switch2)
				pCmd->viewangles.y -= (60 - rand() % 20);
			else
				pCmd->viewangles.y -= (120 - rand() % 20);
			switch2 = !switch2;
		}
		else if (rando > 375)
		{
			if (switch2)
				pCmd->viewangles.y += (60 + rand() % 20);
			else
				pCmd->viewangles.y += (120 + rand() % 20);
			switch2 = !switch2;
		}
		else
			pCmd->viewangles.y -= (180 + rand() % 45) - rand() % 20;
	}
	break;
	case 4:
	{

		pCmd->viewangles.y = (pLocal->GetLowerBodyYaw() + 140 + rand() % 123) - rand() % 20;
	}
	break;
	case 5:
	{
		if (NextLBYUpdate)
			pCmd->viewangles.y = (pLocal->GetLowerBodyYaw() - 150) - rand() % 60;
		else
			pCmd->viewangles.y = (pLocal->GetLowerBodyYaw() + 150) + rand() % 60;
	}
	break;
	case 6:
	{
		if (switch2)
		{
			pCmd->viewangles.y = -80.00 + rand() % 150;
		}
		else
			pCmd->viewangles.y = 80.00 - rand() % 150;

	}
	break;
	case 7:
	{
		y2 += spinspeed;

		if (y2 >= 175)
			y2 = -175;

		pCmd->viewangles.y = y2;
	}
	break;
	case 8:
	{
		int rnd = rand() % 70;
		static bool cFlipYaw;
		if (rnd < 20) rnd = 20;
		static bool cFlipYaw2;
		int rndb = rand() % 170;



		y2 += spinspeed;

		if (y2 >= 119)
			y2 = -119;

		if (rndb < 25) rnd = 25;

		if (pLocal->GetHealth() > 45)
		{
			if (switch2)
				pCmd->viewangles.y = hackManager.pLocal()->GetEyeAnglesXY()->y - (60 - rndb);
			else
				pCmd->viewangles.y = hackManager.pLocal()->GetEyeAnglesXY()->y + (140 + rndb);
			switch2 = !switch2;
		}
		else if (pLocal->GetHealth() <= 45 && !IsAbleToShoot)
		{
			pCmd->viewangles.y = hackManager.pLocal()->GetEyeAnglesXY()->y + cFlipYaw2 ? y2 : y2 + 60;

		}
		else if (CanOpenFire)
		{
			if (NextLBYUpdate())
			{
				float transgender = pLocal->GetLowerBodyYaw() - 119.5f;

				static int y2 = transgender;
				int spinBotSpeedFast = 35 + rand() % 10;
				//
				y2 += spinBotSpeedFast;

				if (y2 >= pLocal->GetEyeAnglesXY()->y - 35)
					y2 = pLocal->GetEyeAnglesXY()->y - 90;

				pCmd->viewangles.y = y2;


			}
			else
			{
				float transgender = pLocal->GetLowerBodyYaw() - 60;
				static int y2 = transgender;
				int spinBotSpeedFast = 45 + rand() % 15;

				y2 += spinBotSpeedFast;

				if (y2 >= pLocal->GetEyeAnglesXY()->y - 55)
					y2 = pLocal->GetEyeAnglesXY()->y - 100;

				pCmd->viewangles.y = y2;
			}
		}

		else
		{
			if (switch2)
				pCmd->viewangles.y = hackManager.pLocal()->GetEyeAnglesXY()->y + 160 + rnd;
			else
				pCmd->viewangles.y = hackManager.pLocal()->GetEyeAnglesXY()->y - 130 - rnd;
			switch2 = !switch2;
		}



	}
	break;
	case 9:
		pCmd->viewangles.y + 180.000000; break;
	case 10:
		pCmd->viewangles.y + 0; break;





	}



}



void gshdgkjshgsuighsg::DoAntiAim(CUserCmd *pCmd, bool &bSendPacket)
{
	PredictBypass();
	IClientEntity* pLocal = hackManager.pLocal();
	static int ChokedPackets = -1;
	ChokedPackets++;
	if ((pCmd->buttons & IN_USE) || pLocal->GetMoveType() == MOVETYPE_LADDER)
		return;
	PredictBypass();
	if (IsAimStepping || pCmd->buttons & IN_ATTACK)
		return;
	PredictBypass();
	CBaseCombatWeapon* pWeapon = (CBaseCombatWeapon*)Interfaces::EntList->GetClientEntityFromHandle(hackManager.pLocal()->GetActiveWeaponHandle());
	if (pWeapon)
	{
		PredictBypass();
		CSWeaponInfo* pWeaponInfo = pWeapon->GetCSWpnData();
		PredictBypass();
		if (!GameUtils::IsBallisticWeapon(pWeapon))
		{
			PredictBypass();
			if (!CanOpenFire() || pCmd->buttons & IN_ATTACK2)
				return;
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
	int flip = (int)floorf(Interfaces::Globals->curtime / 1.1) % 2;
	static bool bFlipYaw;
	static bool wilupdate;
	float flInterval = Interfaces::Globals->interval_per_tick;
	float flTickcount = pCmd->tick_count;
	float flTime = flInterval * flTickcount;
	if (std::fmod(flTime, 1) == 0.f)
		bFlipYaw = !bFlipYaw;
	static float LastLBYUpdateTime = 0;

	PredictBypass();

	FakeWalk(pCmd, bSendPacket);
	PredictBypass();
	switch (options.Rage.pitchaa)
	{
	case 0:
		break;
	case 1:
		pCmd->viewangles.x = 88.f;
		break;
	case 2:
		pCmd->viewangles.x = -88.f;
		break;
	case 3:
		pCmd->viewangles.x = 1073.000000;
		break;
	case 4:
		pCmd->viewangles.x = -1073.000000;
		break;
	case 5: // stay freaky
	{
		pCmd->viewangles.x = 75.000000 + rand() % 17;
	

	}
	
		break;
	case 6:
	{
		pCmd->viewangles.x = 0.f;
	}
	break;
	case 7: 
	{
		pCmd->viewangles.x = bSendPacket ? 0.f : 89.f;
	}
	break;
	case 8:
	{
		if (pCmd->viewangles.x > 9.f && LastLBYUpdateTime)
			pCmd->viewangles.x = 0.f + rand() % 4;
		else if (LastLBYUpdateTime)
			pCmd->viewangles.x = pCmd->viewangles.x / 2;
		else
			pCmd->viewangles.x = 0.f;
	}
	break;
	case 9:
	{
		pCmd->viewangles.x = LastLBYUpdateTime ? 89.f : -89.f;
	}
	break;
	case 10:
	{
		pCmd->viewangles.x = pCmd->viewangles.x / 2;
	}
	break;
	}PredictBypass();
	PredictBypass();
	if (options.Rage.MainSwitch && options.Rage.enableaa)
	{
		PredictBypass();
		static bool respecc = false;
		float antilby = options.HvH.DeltaStand;
		float moveantilby = options.HvH.DeltaMove;
		PredictBypass();
		if (options.HvH.FakeLagMode != 0)
		{
			PredictBypass();
			static int fake = 0;
			PredictBypass();
			bool onground;
			if (pLocal->IsScoped())
				onground = true;
			else
				onground = false;
			PredictBypass();
			if (options.HvH.FakeLagMode == 0)
			{
				PredictBypass();
			}
			else if (options.HvH.FakeLagMode == 1)
			{
				PredictBypass();
				int tickstochoke = 4;
				//	tickstochoke = std::min<int>(Menu::Window.aabuild.FakeLagChoke.GetValue(), 13);
				if (ChokedPackets >= 3)
				{
					fake = tickstochoke + 1;
				}
				PredictBypass();

				if (fake < tickstochoke)
				{
					bSendPacket = false;
					PredictBypass();
					DoRealAA(pCmd, pLocal, bSendPacket);

					if (!respecc)
					{
						if (fake == 0 && pLocal->GetVelocity().Length2D() <= 1)
						{

							if (NextLBYUpdate())
							{
								
								pCmd->viewangles.y += antilby;

								pCmd->viewangles.y -= antilby;
								{
									pCmd->viewangles.y += antilby;
								}
							}
						}
						else if (fake == 0 && pLocal->GetVelocity().Length2D() > 1)
						{

							if (NextMovingLBYUpdate())
							{

								pCmd->viewangles.y += moveantilby;

								pCmd->viewangles.y -= moveantilby;
						   	{
									pCmd->viewangles.y += moveantilby;
								}
							}

						}
					}
					fake++;
					PredictBypass();
				}
				else
				{
					bSendPacket = true;
					DoFakeAA(pCmd, bSendPacket, pLocal);
					fake = 0;
				}
				if (!bSendPacket)
					++ChokedPackets;
				else
					ChokedPackets = 0;

				if (ChokedPackets > 3)
					bSendPacket = true;
				PredictBypass();

			}
			else if (options.HvH.FakeLagMode == 2)
			{
				int tickstochoke = 9;
				PredictBypass();
				//	tickstochoke = std::min<int>(Menu::Window.aabuild.FakeLagChoke.GetValue(), 13);
				if (ChokedPackets >= 8)
				{
					fake = tickstochoke + 1;
				}

				PredictBypass();
				if (fake < tickstochoke)
				{
					PredictBypass();
					bSendPacket = false;
					PredictBypass();
					DoRealAA(pCmd, pLocal, bSendPacket);
					PredictBypass();
					if (!respecc)
					{
						if (fake == 0 && pLocal->GetVelocity().Length2D() <= 1)
						{

							if (NextLBYUpdate())
							{

								pCmd->viewangles.y += antilby;

								pCmd->viewangles.y -= antilby;
								{
									pCmd->viewangles.y += antilby;
								}
								PredictBypass();
							}
						}
						else if (fake == 0 && pLocal->GetVelocity().Length2D() >= 1)
						{

							if (NextMovingLBYUpdate())
							{
								PredictBypass();
								PredictBypass();
								pCmd->viewangles.y += moveantilby;
								PredictBypass();
								pCmd->viewangles.y -= moveantilby;
								{
									pCmd->viewangles.y += moveantilby;
									PredictBypass();
								}
								PredictBypass();
							}
							PredictBypass();
						}
						PredictBypass();
					}
					fake++;
					PredictBypass();

				}
				else
				{
					bSendPacket = true;
					DoFakeAA(pCmd, bSendPacket, pLocal);
					fake = 0;
					PredictBypass();
				}

				PredictBypass();
				if (!bSendPacket)
					++ChokedPackets;
				else
					ChokedPackets = 0;
				PredictBypass();
				if (ChokedPackets > 8)
					bSendPacket = true;
				PredictBypass();
			}
			else if (options.HvH.FakeLagMode == 3)
			{
				int tickstochoke = 12;
				PredictBypass();
				//	tickstochoke = std::min<int>(Menu::Window.aabuild.FakeLagChoke.GetValue(), 13);
				if (ChokedPackets >= 11)
				{
					fake = tickstochoke + 1;
				}
				PredictBypass();
			
				if (fake < tickstochoke)
				{
					bSendPacket = false;

					DoRealAA(pCmd, pLocal, bSendPacket);
					if (respecc)
					{
						if (fake == 0 && pLocal->GetVelocity().Length2D() <= 0.5)
						{
							if (fake == 0 && pLocal->GetVelocity().Length2D() <= 1)
							{
								PredictBypass();
								if (NextLBYUpdate())
								{
									PredictBypass();

									pCmd->viewangles.y += antilby;
									PredictBypass();
									pCmd->viewangles.y -= antilby;
									{
										pCmd->viewangles.y += antilby;
									}
									PredictBypass();
								}
								PredictBypass();
							}
							else if (fake == 0 && pLocal->GetVelocity().Length2D() > 1)
							{
								PredictBypass();
								if (NextMovingLBYUpdate())
								{
									PredictBypass();
									pCmd->viewangles.y += moveantilby;
									PredictBypass();
									pCmd->viewangles.y -= moveantilby;
									{
										pCmd->viewangles.y += moveantilby;
									}
									PredictBypass();
								}
								PredictBypass();
							}
							PredictBypass();
						}
						fake++;
						PredictBypass();

					}
					else
					{
						PredictBypass();
						bSendPacket = true;
						DoFakeAA(pCmd, bSendPacket, pLocal);
						fake = 0;
						PredictBypass();
					}
					PredictBypass();

					if (!bSendPacket)
						++ChokedPackets;
					else
						ChokedPackets = 0;
					PredictBypass();
					if (ChokedPackets >= 11)
						bSendPacket = true;
					PredictBypass();
				}
				PredictBypass();

			}

			PredictBypass();
			if (flipAA)
			{
				pCmd->viewangles.y - 135;
				PredictBypass();
			}
			PredictBypass();
		}
		PredictBypass();
	}
	PredictBypass();
}

