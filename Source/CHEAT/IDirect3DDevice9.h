#pragma once
#include "SDK.h"
#include "Renderer.h"
#include "RenderManager.h"
#include <d3d9.h>
#include "dropboxes.h"
#include "Configs.hpp"
#include "Options.h"
#include <unordered_map>

uint8_t* m_present;
uint8_t* m_reset;

IDirect3DDevice9* d3d9_device;
typedef HRESULT(__stdcall *EndScene_t) (IDirect3DDevice9*);

typedef HRESULT(__stdcall *Reset_t) (IDirect3DDevice9*, D3DPRESENT_PARAMETERS*);

EndScene_t	oEndScene = nullptr;
Reset_t		oReset = nullptr;

HRESULT __stdcall hkReset(IDirect3DDevice9* thisptr, D3DPRESENT_PARAMETERS* params) {

	if (!renderer->IsReady())
		return oReset(thisptr, params);


	ImGui_ImplDX9_InvalidateDeviceObjects();




	renderer->~Renderer();

	HRESULT result = oReset(thisptr, params);

	ImGui_ImplDX9_CreateDeviceObjects();

	renderer->Initialize(FindWindowA("Valve001", NULL), thisptr);
	Render::Initialise();

	return result;
}

void Light(ImGuiStyle* dst)
{
	ImGuiStyle* style = dst ? dst : &ImGui::GetStyle();

	ImVec4* colors = style->Colors;



	colors[ImGuiCol_Text] = ImVec4(0.00f, 0.00f, 0.00f, 1.00f);

	colors[ImGuiCol_TextDisabled] = ImVec4(0.60f, 0.60f, 0.60f, 1.00f);

	//colors[ImGuiCol_TextHovered]          = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

	//colors[ImGuiCol_TextActive]           = ImVec4(1.00f, 1.00f, 0.00f, 1.00f);

	colors[ImGuiCol_WindowBg] = ImVec4(0.94f, 0.94f, 0.94f, 1.00f);


	colors[ImGuiCol_PopupBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.98f);

	colors[ImGuiCol_Border] = ImVec4(0.00f, 0.00f, 0.00f, 0.30f);

	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);

	colors[ImGuiCol_FrameBg] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);

	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);

	colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);

	colors[ImGuiCol_TitleBg] = ImVec4(0.96f, 0.96f, 0.96f, 1.00f);

	colors[ImGuiCol_TitleBgActive] = ImVec4(0.82f, 0.82f, 0.82f, 1.00f);

	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(1.00f, 1.00f, 1.00f, 0.51f);

	colors[ImGuiCol_MenuBarBg] = ImVec4(0.86f, 0.86f, 0.86f, 1.00f);

	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.98f, 0.98f, 0.98f, 0.53f);

	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.69f, 0.69f, 0.69f, 0.80f);

	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.49f, 0.49f, 0.49f, 0.80f);

	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.49f, 0.49f, 0.49f, 1.00f);

	colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);

	colors[ImGuiCol_SliderGrab] = ImVec4(0.26f, 0.59f, 0.98f, 0.78f);

	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.46f, 0.54f, 0.80f, 0.60f);

	colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);

	colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);

	colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);

	colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);

	colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);

	colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);



	colors[ImGuiCol_ResizeGrip] = ImVec4(0.80f, 0.80f, 0.80f, 0.56f);

	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);

	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);

	colors[ImGuiCol_CloseButton] = ImVec4(0.59f, 0.59f, 0.59f, 0.50f);

	colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);

	colors[ImGuiCol_CloseButtonActive] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);

	colors[ImGuiCol_PlotLines] = ImVec4(0.39f, 0.39f, 0.39f, 1.00f);

	colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);

	colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);

	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.45f, 0.00f, 1.00f);

	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);

	colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.20f, 0.20f, 0.20f, 0.35f);


}

void Dark(ImGuiStyle* dst)
{
	ImGuiStyle* style = dst ? dst : &ImGui::GetStyle();
	ImVec4* colors = style->Colors;

	colors[ImGuiCol_Text] = ImVec4(1.00f, 1.00f, 1.00f, 1.00f);
	colors[ImGuiCol_TextDisabled] = ImVec4(0.50f, 0.50f, 0.50f, 1.00f);
	colors[ImGuiCol_WindowBg] = ImVec4(0.06f, 0.06f, 0.06f, 0.94f);
//	colors[ImGuiCol_ChildBg] = ImVec4(1.00f, 1.00f, 1.00f, 0.00f);
	colors[ImGuiCol_PopupBg] = ImVec4(0.08f, 0.08f, 0.08f, 0.94f);
	colors[ImGuiCol_Border] = ImVec4(0.43f, 0.43f, 0.50f, 0.50f);
	colors[ImGuiCol_BorderShadow] = ImVec4(0.00f, 0.00f, 0.00f, 0.00f);
	colors[ImGuiCol_FrameBg] = ImVec4(0.16f, 0.29f, 0.48f, 0.54f);
	colors[ImGuiCol_FrameBgHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_FrameBgActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_TitleBg] = ImVec4(0.04f, 0.04f, 0.04f, 1.00f);
	colors[ImGuiCol_TitleBgActive] = ImVec4(0.16f, 0.29f, 0.48f, 1.00f);
	colors[ImGuiCol_TitleBgCollapsed] = ImVec4(0.00f, 0.00f, 0.00f, 0.51f);
	colors[ImGuiCol_MenuBarBg] = ImVec4(0.14f, 0.14f, 0.14f, 1.00f);
	colors[ImGuiCol_ScrollbarBg] = ImVec4(0.02f, 0.02f, 0.02f, 0.53f);
	colors[ImGuiCol_ScrollbarGrab] = ImVec4(0.31f, 0.31f, 0.31f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabHovered] = ImVec4(0.41f, 0.41f, 0.41f, 1.00f);
	colors[ImGuiCol_ScrollbarGrabActive] = ImVec4(0.51f, 0.51f, 0.51f, 1.00f);
	colors[ImGuiCol_CheckMark] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_SliderGrab] = ImVec4(0.24f, 0.52f, 0.88f, 1.00f);
	colors[ImGuiCol_SliderGrabActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_Button] = ImVec4(0.26f, 0.59f, 0.98f, 0.40f);
	colors[ImGuiCol_ButtonHovered] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	colors[ImGuiCol_ButtonActive] = ImVec4(0.06f, 0.53f, 0.98f, 1.00f);
	colors[ImGuiCol_Header] = ImVec4(0.26f, 0.59f, 0.98f, 0.31f);
	colors[ImGuiCol_HeaderHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.80f);
	colors[ImGuiCol_HeaderActive] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	//colors[ImGuiCol_Separator] = colors[ImGuiCol_Border];
//	colors[ImGuiCol_SeparatorHovered] = ImVec4(0.10f, 0.40f, 0.75f, 0.78f);
//	colors[ImGuiCol_SeparatorActive] = ImVec4(0.10f, 0.40f, 0.75f, 1.00f);
	colors[ImGuiCol_ResizeGrip] = ImVec4(0.26f, 0.59f, 0.98f, 0.25f);
	colors[ImGuiCol_ResizeGripHovered] = ImVec4(0.26f, 0.59f, 0.98f, 0.67f);
	colors[ImGuiCol_ResizeGripActive] = ImVec4(0.26f, 0.59f, 0.98f, 0.95f);
	colors[ImGuiCol_CloseButton] = ImVec4(0.41f, 0.41f, 0.41f, 0.50f);
	colors[ImGuiCol_CloseButtonHovered] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	colors[ImGuiCol_CloseButtonActive] = ImVec4(0.98f, 0.39f, 0.36f, 1.00f);
	colors[ImGuiCol_PlotLines] = ImVec4(0.61f, 0.61f, 0.61f, 1.00f);
	colors[ImGuiCol_PlotLinesHovered] = ImVec4(1.00f, 0.43f, 0.35f, 1.00f);
	colors[ImGuiCol_PlotHistogram] = ImVec4(0.90f, 0.70f, 0.00f, 1.00f);
	colors[ImGuiCol_PlotHistogramHovered] = ImVec4(1.00f, 0.60f, 0.00f, 1.00f);
	colors[ImGuiCol_TextSelectedBg] = ImVec4(0.26f, 0.59f, 0.98f, 0.35f);
	colors[ImGuiCol_ModalWindowDarkening] = ImVec4(0.80f, 0.80f, 0.80f, 0.35f);
	//colors[ImGuiCol_DragDropTarget] = ImVec4(1.00f, 1.00f, 0.00f, 0.90f);
//	colors[ImGuiCol_NavHighlight] = ImVec4(0.26f, 0.59f, 0.98f, 1.00f);
	//colors[ImGuiCol_NavWindowingHighlight] = ImVec4(1.00f, 1.00f, 1.00f, 0.70f);
}

void Theme_Init(IDirect3DDevice9* pDevice)
{
	ImGuiIO& io = ImGui::GetIO();
	io.DeltaTime = 1.f / 60.f;
	D3DDEVICE_CREATION_PARAMETERS d3dcp{ 0 };
	pDevice->GetCreationParameters(&d3dcp);

	auto& style = ImGui::GetStyle();

	style.Alpha = 1.0f;
	style.WindowPadding = ImVec2(8, 8);
	style.WindowMinSize = ImVec2(32, 32);
	style.WindowRounding = 9.0f;
	style.WindowTitleAlign = ImVec2(0.5f, 0.5f);
	style.ChildWindowRounding = 0.0f;
	style.FramePadding = ImVec2(4, 3);
	style.FrameRounding = 2.3f;
	style.ItemSpacing = ImVec2(8, 4);
	style.ItemInnerSpacing = ImVec2(4, 4);
	style.TouchExtraPadding = ImVec2(0, 0);
	style.IndentSpacing = 21.0f;
	style.ColumnsMinSpacing = 3.0f;
	style.ScrollbarSize = 12.0f;
	style.ScrollbarRounding = 0.0f;
	style.GrabMinSize = 5.0f;
	style.GrabRounding = 0.0f;
	style.ButtonTextAlign = ImVec2(0.5f, 0.5f);
	style.DisplayWindowPadding = ImVec2(22, 22);
	style.DisplaySafeAreaPadding = ImVec2(4, 4);
	style.AntiAliasedLines = true;
	style.AntiAliasedShapes = true;
	style.CurveTessellationTol = 1.25f;

	Dark(NULL);
	//Light(NULL);
}



void DrawSpectatingYou()
{
	auto& style = ImGui::GetStyle();
	style.Alpha = 0.9f;
	//ImGui::PushFont(Global::fDefault);
	ImGui::SetNextWindowSize(ImVec2(130, 150), ImGuiSetCond_FirstUseEver);
	if (ImGui::Begin("Spectating Me", NULL, ImGuiWindowFlags_NoCollapse))
	{
		IClientEntity *pLocal = hackManager.pLocal();
		// Loop through all active entitys
		for (int i = 0; i < Interfaces::EntList->GetHighestEntityIndex(); i++)
		{
			// Get the entity
			IClientEntity *pEntity = Interfaces::EntList->GetClientEntity(i);
			player_info_t pinfo;
			// The entity isn't some laggy peice of shit or something
			if (pEntity &&  pEntity != pLocal)
			{
				if (Interfaces::Engine->GetPlayerInfo(i, &pinfo) && !pEntity->IsAlive() && !pEntity->IsDormant())
				{
					HANDLE obs = pEntity->GetObserverTargetHandle();
					if (obs)
					{
						IClientEntity *pTarget = Interfaces::EntList->GetClientEntityFromHandle(obs);
						player_info_t pinfo2;
						if (pTarget)
						{
							if (Interfaces::Engine->GetPlayerInfo(pTarget->GetIndex(), &pinfo2))
							{
								if (pTarget->GetIndex() == pLocal->GetIndex())
									ImGui::Text("%s", pinfo.name);
							}
						}
					}
				}
			}
		}
	}
	ImGui::End();
}

player_info_t GetInfo1(int Index) {
	player_info_t Info;
	Interfaces::Engine->GetPlayerInfo(Index, &Info);
	return Info;
}

PlayerList_t PlayerList[65];
#define arrayss(_ARR)  ((int)(sizeof(_ARR)/sizeof(*_ARR)))
typedef HRESULT(_stdcall *Present_T)(void*, const RECT*, RECT*, HWND, RGNDATA*);
Present_T oPresent;
HRESULT _stdcall hkPresent(LPDIRECT3DDEVICE9 pDevice, RECT* pSourceRect, RECT* pDestRect, HWND hDestWindowOverride, RGNDATA* pDirtyRegion)
{
	if (!renderer->IsReady())
	{
		Theme_Init(pDevice);
		renderer->Initialize(FindWindowA("Valve001", NULL), pDevice);
	}
	static bool mouse_enabled = false;
	pDevice->SetRenderState(D3DRS_COLORWRITEENABLE, 0xFFFFFFFF);
	bool is_renderer_active = renderer->IsActive();

	if (is_renderer_active) {
		if (mouse_enabled) {
			Interfaces::Engine->ClientCmd_Unrestricted("cl_mouseenable 0");
			mouse_enabled = false;
		}
	}
	else {
		if (!mouse_enabled) {
			Interfaces::Engine->ClientCmd_Unrestricted("cl_mouseenable 1");
			mouse_enabled = true;
		}
	}

	ImGui::GetIO().MouseDrawCursor = is_renderer_active;

	ImGui_ImplDX9_NewFrame();

	if (is_renderer_active)
	{
	//	ImGui::ShowTestWindow();
		auto& style = ImGui::GetStyle();
		style.Alpha = 1.0f;
		ImGui::SetNextWindowSize(ImVec2(960, 446), ImGuiSetCond_FirstUseEver);
		if (ImGui::Begin(("cringe.tk - mirror.tk v4 with imgui"), NULL, ImVec2(960, 446), 1.0f, ImGuiWindowFlags_NoScrollbar | ImGuiWindowFlags_NoCollapse))
		{

			const char* tabs[] = {
				"Ragebot",
				"Playerlist",
				"Visuals",
				"Misc",
				"HvH"
			};

			for (int i = 0; i < arrayss(tabs); i++)
			{

				//ImGui::GetStyle().Colors[ImGuiCol_Button] = ImColor(54, 54, 54, 255);

				if (ImGui::Button(tabs[i], ImVec2(ImGui::GetWindowSize().x / arrayss(tabs) - 9, 0)))
					options.Menu.ActualTab = i;

				//ImGui::GetStyle().Colors[ImGuiCol_Button] = ImColor(54, 54, 54, 255);

				if (i < arrayss(tabs) - 1)
					ImGui::SameLine();
			}

			if (options.Menu.ActualTab == 0) {
				ImGui::Checkbox(("Active"), &options.Rage.MainSwitch);

				ImGui::Columns(3, NULL, true);
				{
					ImGui::Text("General"); ImGui::NextColumn(); ImGui::Text("Accuracy"); ImGui::NextColumn(); ImGui::Text("Anti-Aim"); ImGui::NextColumn(); ImGui::Separator();
					ImGui::Checkbox("Enable", &options.Rage.Enabled);
					ImGui::SliderFloat("##aimbotfovslider", &options.Rage.AimFOV, 0.f, 180.f, "Field-of-View: %.f");
					ImGui::Checkbox("Auto-Fire", &options.Rage.AutoFiring);
					ImGui::Checkbox("Silent", &options.Rage.SilentTYPE);
					ImGui::Combo("Bone Selection", &options.Rage.Hitbone1, hitbones_r, ARRAYSIZE(hitbones_r));
					ImGui::Combo("Target Selection", &options.Rage.selectiontrgt, slct_trgt, ARRAYSIZE(slct_trgt));
					ImGui::Checkbox("Multi-Point", &options.Rage.MultiPoint_mb);
					ImGui::SliderFloat("##multipointslider", &options.Rage.Pointscale, 0.f, 1.f, "Pointscale: %.2f");

				}
				ImGui::NextColumn();
				{
					ImGui::Combo("Control", &options.Rage.AcurracyControl, accuracycontrol, ARRAYSIZE(accuracycontrol));
					ImGui::Checkbox("Autowall", &options.Rage.autowall);
					ImGui::SliderFloat("##mindamageslider", &options.Rage.autowalldmg, 0.f, 100.f, "Min. Damage: %.f");
					ImGui::SliderFloat("##hitchancevalueslider", &options.Rage.hitchanceval, 0.f, 100.f, "Hitchance: %.2f");
					ImGui::Combo("Resolver", &options.Rage.resolver, resolvers, ARRAYSIZE(resolvers));
					ImGui::NewLine();
					ImGui::SameLine(15);
					ImGui::Hotkey("Force Brute", &options.Rage.flipangle, ImVec2(50, 23));
					if (ImGui::IsItemHovered())
						ImGui::SetTooltip("Works only with Prediction and Prediction-V2 Resolver.");
					ImGui::NewLine();
					ImGui::SameLine(15);
					ImGui::SliderInt("##bruteafterxshotsslider", &options.Rage.bruteafter, 0, 20, "Brute After %.f Shots");
					ImGui::Checkbox("Position Adjustment", &options.Rage.PosAdjustment);
					ImGui::Checkbox("Position Log", &options.Rage.bcktrck);
					ImGui::Checkbox("Position Correction", &options.Rage.lagfix);
					ImGui::Combo("Prefer Body", &options.Rage.preferbaim, preferbaiming, ARRAYSIZE(preferbaiming));
					ImGui::SliderInt("##BaimAfterXShotsslider", &options.Rage.BaimAfterXShots, 0, 20, "Baim After %.f Shots");;
					ImGui::SliderInt("##BaimIFHpIsUnderXslider", &options.Rage.BaimIFUnderXHP, 0, 100, "Baim if HP < %.f");
				}
				ImGui::NextColumn();
				{
					ImGui::Checkbox("Enabled", &options.Rage.enableaa);
					ImGui::Combo("Pitch", &options.Rage.pitchaa, pitch_style, ARRAYSIZE(pitch_style));
					ImGui::Combo("Real Yaw", &options.Rage.realyaa, realy_style, ARRAYSIZE(realy_style));
					ImGui::Combo("Fake Yaw", &options.Rage.fakeyyaa, fakey_style, ARRAYSIZE(fakey_style));
					ImGui::Combo("Move Yaw", &options.Rage.moveyaa, movey_style, ARRAYSIZE(movey_style));
					ImGui::Hotkey("Switch Left", &options.Rage.leftkey);
					ImGui::Hotkey("Switch Right", &options.Rage.rightkey);
				}
			}
			else if (options.Menu.ActualTab == 1)
			{
				char *playersNicknames[15];
				int playerIndxes[15];
				auto localplayer = static_cast<IClientEntity*>(Interfaces::EntList->GetClientEntity(Interfaces::Engine->GetLocalPlayer()));
				int i = 0;

				if (Interfaces::Engine->IsInGame())
				{
					for (INT ax = 1; ax < Interfaces::Engine->GetMaxClients(); ++ax)
					{
						if (PlayerList[ax].pPlayer != NULL)
						{
							if (PlayerList[ax].pPlayer->GetTeamNum() != localplayer->GetTeamNum())
							{
								player_info_t info = GetInfo1(PlayerList[ax].pPlayer->GetIndex());
								playersNicknames[i] = new char[(sizeof(info.name) / sizeof(char)) + 1];
								strcpy(playersNicknames[i], info.name);
								playerIndxes[i] = ax;
								i++;
							}

						}
					}

					if (PlayerList[playerIndxes[options.PlayerlistOpt.selectedplayer]].pPlayer != NULL && playersNicknames[0] != nullptr)
					{
						const char* listbox_items32[] = { "None", "Down", "UP" };
						const char* listbox_items64[] = { "None", "Inverse", "Left", "Right" };
						ImGui::Combo("Player", &options.PlayerlistOpt.selectedplayer, playersNicknames, i);
						ImGui::Combo("Correct Pitch", &PlayerList[playerIndxes[options.PlayerlistOpt.selectedplayer]].bFixX, listbox_items32, ARRAYSIZE(listbox_items32));
						ImGui::Combo("Correct Jitter-Angle", &PlayerList[playerIndxes[options.PlayerlistOpt.selectedplayer]].bChangeJitter, listbox_items64, ARRAYSIZE(listbox_items64));
						ImGui::Checkbox("Force Baim", &PlayerList[playerIndxes[options.PlayerlistOpt.selectedplayer]].bBaim);
						//ImGui::Checkbox("Priority Target", &PlayerList[playerIndxes[options.PlayerlistOpt.selectedplayer]].bHighPriority);
					}
				}
				else
					ImGui::Text("Join the server first!");
			}
			else if (options.Menu.ActualTab == 2)
			{
				ImGui::Checkbox("Active", &options.ESP.Active);
				ImGui::Columns(3, NULL, true);
				{
					ImGui::Text("ESP"); ImGui::NextColumn(); ImGui::Text("Others"); ImGui::NextColumn(); ImGui::Text("Render"); ImGui::NextColumn(); ImGui::Separator();
					ImGui::Combo("Box", &options.ESP.BoxEsp, boxes, ARRAYSIZE(boxes));
					ImGui::Checkbox("Health Bar", &options.ESP.HPBar);
					ImGui::Checkbox("Name", &options.ESP.name);
					ImGui::Checkbox("Armor", &options.ESP.armor);
					ImGui::Checkbox("Weapon", &options.ESP.weapon);
				}
				ImGui::NextColumn();
				{
					ImGui::Selectable("+ Filter: Players", &options.ESP.FilterPlayers);
					ImGui::Selectable("+ Filter: Enemy Only", &options.ESP.FilterEnemy);
					ImGui::Selectable("+ Filter: Weapons", &options.ESP.FilterWeapons);
					ImGui::Selectable("+ Filter: Explosives", &options.ESP.FilterExplosives);
					ImGui::Selectable("+ Removable: Visual Recoil", &options.ESP.removevisualrecoil);
					ImGui::Selectable("+ Removable: Scope Border", &options.ESP.removescopeborder);
					ImGui::Selectable("+ Removable: Flash", &options.ESP.removeflash);
					ImGui::Selectable("+ Removable: Smoke", &options.ESP.removesmoke);
				}
				ImGui::NextColumn();
				{
					ImGui::SliderFloat("##fovmodslider", &options.ESP.fovchanger, 0.f, 135.f, "FOV Mod: %.2f");
					ImGui::Combo("Ghost Angle", &options.ESP.fakeanglechams, ghostmode, ARRAYSIZE(ghostmode));
					ImGui::Checkbox("Spread Circle", &options.ESP.spreadcrs);
					ImGui::Checkbox("Spectating List", &options.ESP.speclist);
					ImGui::Checkbox("Event Logs", &options.ESP.logs);
					//ImGui::Checkbox("Bullet Tracer", &options.ESP.createtracer);
				}
			}
			else if (options.Menu.ActualTab == 3)
			{
				ImGui::Columns(3, NULL, true);
				{
					ImGui::Text("General"); ImGui::NextColumn(); ImGui::Text("Misc"); ImGui::NextColumn(); ImGui::Text("Others"); ImGui::NextColumn(); ImGui::Separator();
					ImGui::Checkbox("Clamp Angles", &options.Misc.SafeMode);
					if (ImGui::IsItemHovered())
						ImGui::SetTooltip("Do not toggle off if on secured server!");
					ImGui::Checkbox("Auto-Jump", &options.Misc.autojumping);
					ImGui::Combo("Auto-Strafer", &options.Misc.actualstrafer, autostrafer, ARRAYSIZE(autostrafer));
				}
				ImGui::NextColumn();
				{
					ImGui::Checkbox("Thirdperson", &options.Misc.thirdperson);
					ImGui::SameLine();
					ImGui::Hotkey("##keybindtp", &options.Misc.tpkey1, ImVec2(50, 23));
					ImGui::NewLine();
					ImGui::SameLine(15);
					ImGui::Combo("Angle", &options.Misc.modetp, thirdpersonmode, ARRAYSIZE(thirdpersonmode));
					ImGui::Checkbox("Enable Knife-Changer", &options.Misc.enableknife);
					ImGui::NewLine();
					ImGui::SameLine(15);
					ImGui::Combo("##skinchangerknife", &options.Misc.skinknife, knifes, ARRAYSIZE(knifes));
					ImGui::Checkbox("Clantag", &options.Misc.clantag);
					ImGui::NewLine();
					ImGui::SameLine(15);
					ImGui::Checkbox("Animated", &options.Misc.animated_clantag);
				}
				ImGui::NextColumn();
				{
					ImGui::PushItemWidth(120);
					ImGui::InputText("File Name", &options.Menu.Configs.ConfigFile, 135);
					if (ImGui::Button("Save")) Config->Save();
					ImGui::SameLine();
					if (ImGui::Button("Load")) Config->Load();
				}
			}
			else if (options.Menu.ActualTab == 4)
			{
				ImGui::Columns(2, NULL, true);
				{
					ImGui::Text("Fakelag"); ImGui::NextColumn(); ImGui::Text("LBY Breaker"); ImGui::NextColumn(); ImGui::Separator();
					ImGui::Combo("Style", &options.HvH.FakeLagMode, fakelagmode, ARRAYSIZE(fakelagmode));
				}
				ImGui::NextColumn();
				{
					ImGui::SliderFloat("##deltastandslider", &options.HvH.DeltaStand, 0.f, 180.f, "Delta Stand: %.f");
					ImGui::SliderFloat("##deltamoveslider", &options.HvH.DeltaMove, 0.f, 180.f, "Delta Move: %.f");
				}
			}

		} ImGui::End();
		if (options.ESP.speclist)
			DrawSpectatingYou();
	}
	else {
		if (options.ESP.speclist)
			DrawSpectatingYou();
	}

	ImGui::Render();

	return oPresent(pDevice, pSourceRect, pDestRect, hDestWindowOverride, pDirtyRegion);
}
